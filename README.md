# sequence-informatics

This is a repository of code intended for learning bio-sequence informatics.
It started out as a book, readiab, and is now heavily modified.
Each python file is actually a jupyter notebook (in jupytext format).

To read:

0. `git clone https://gitlab.com/bio-data/sequence-informatics.git`
1. Install jupyter notebook and jupytext
2. Open each of the chapters, which are jupytext python files, in jupyter notebook
3. Execute it as you would a normal jupyter notebook

Alternatively, you can trace the code in these files:

`pudb chapter.py`

Or, execute them directly:

`python3 chapeter.py`

I do not have all the future notebook scripts in the repo yet. 
As they are completed, I will push them.
You can pull the latest changes by simply changing into this directory and typing:

`git pull`
