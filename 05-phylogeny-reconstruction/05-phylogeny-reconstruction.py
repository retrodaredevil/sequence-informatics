# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# Biological success..
# ![](./images/evolution.jpg)
#
# # Phylogenetic reconstruction
# * In this chapter we'll begin to explore the goals, approaches, and challenges for creating phylogenetic trees, or phylogenies.
# * Phylogenies, such as the two presented in Figure 1, represent hypotheses about the evolutionary history of a group of individuals.
#     * Individuals are represented by the *tips* in the tree.
# * Explore an interactive version of the three-domain tree presented in Figure 1b online, through the [Interactive Tree of Life project](http://itol.embl.de/itol.cgi#).
#
# For a highlight of different fields, see:
# * https://en.wikipedia.org/wiki/Computational_phylogenetics
# * https://en.wikipedia.org/wiki/Distance_matrices_in_phylogeny
# * https://en.wikipedia.org/wiki/Phylogenetic_tree
# * https://en.wikipedia.org/wiki/Tree_of_life_(biology)
# * https://en.wikipedia.org/wiki/Cladistics
# * https://en.wikipedia.org/wiki/Systematics
# * https://en.wikipedia.org/wiki/Dendrogram
# * https://en.wikipedia.org/wiki/Cladogram
# * https://en.wikipedia.org/wiki/Phylogenetic_nomenclature
# * https://en.wikipedia.org/wiki/Phylogenetic_network
#
# <img src="./images/Darwins_tree_of_life_1859.png">
#
# Figure 1a: Evolutionary tree presented by Charles Darwin in "On the Origin of Species."
#
# <img src="./images/Pace_Big_Tree.png" width=50%>
#
# Figure 1b: A hypothesis of evolutionary relationships between the three domains of life. This image was created by http://pacelab.colorado.edu/PI_NormPace.html

# %% [markdown]
# ## Why build phylogenies?
# * Reconstructing the phylogeny of a group of individuals is useful for many reasons.
# * Probably the most obvious of these is understanding the evolutionary relationship between a group of organisms.
# * Over the past half-century we've gained great insight into the evolution of part of our species, *Homo sapiens*, by studying features of our closest relatives, both extant (still existing) organisms, such as the *Pan* (chimpanzees), and *Gorilla* genera, and extinct (no longer living) separate species, including *Homo neanderthalensis*, *Homo erectus*, *Homo habilus*, and many species in the *Australopithecus* genus.
#     * Homo neanderthalensis inter-bred with northern dwelling Homo sapiens, who to this day, still possess their DNA in significant quantities (https://en.wikipedia.org/wiki/Neanderthal)
#     * Denisovans did similarly with other populations (https://en.wikipedia.org/wiki/Denisovan)
# * The [Smithsonian Museum's Human Origins Initiative](http://humanorigins.si.edu/) is an excellent resource for learning more about the fascinating subject of human evolution.
#
# ![image1.png](./images/image1.png)
#
# ![image2.png](./images/image2.png)
#
# ![image3.png](./images/image3.png)
#
# ![image4.png](./images/image4.png)

# %% [markdown]
# Evolution is so strange. The ancestors of dolphins started off as sea creatures, then evolved to have legs, only to eventually return to the sea and lose those legs.
#
# Kinda defeets the porpoise, don't you think?
#
# And, the primate's nearest cognitive competitor:
# https://en.wikipedia.org/wiki/Cetacea
#
# Neuron counts
# https://www.frontiersin.org/articles/10.3389/neuro.09.031.2009/full
#
# ![image5.png](./images/image5.png)
#
# Funny stories about dolphins and other cetaceans...

# %% [markdown]
# In this time, we've also improved our understanding of the deeper branches in the tree of life.
# * For example, [Woese and Fox (1977)](http://www.pnas.org/content/74/11/5088.full.pdf) used phylogenetic reconstruction to first illustrate that the "prokaryotes" really represented two ancient lineages which they called the eubacteria and the archaebacteria, which ultimately led to the proposal of a "three domain" tree of life composed of the three deep branching lineages,
#     * the archaea,
#     * the bacteria, and
#     * the eucarya ([Woese, Kandler and Wheelis (1990)](http://www.pnas.org/content/87/12/4576.full.pdf)).
#
# ![](./images/3clades.png)
#
# ![](./images/three-clades.jpg)
#
# ![](./images/domains.jpg)
#
# * Phylogenetic trees such as these are also useful for understanding evolution itself.
# * **In fact, they're so useful that the single image that Charles Darwin found important enough to include in *On the Origin of Species* was the phylogenetic tree presented in Figure 1a.**

# %% [markdown]
# The *individuals* represented at the tips of our trees don't necessarily have to be organisms.
# * We can also study the evolution of genes, which can help us gain a deeper understanding of gene function.
# * Specifically, we can learn about families of related genes.
# * A classic example of this is the globin family, which includes the proteins hemoglobin and myoglobin.
#     * These molecules can reversibly bind oxygen (meaning they can bind to it, and then let go of it).
# * This molecule binds to oxygen where it is present in high concentration (such as in your lung) and releases it where it is present in low concentration (such as in the bicep, where it is ultimately used in the systems that power your arm).
#
# ![](./images/hemoglobin.png)
#
# * Hemoglobin and myoglobin are paralogs, meaning that they are related by a gene duplication and subsequent divergence.
# * If you were to compare an unknown globin sequence to either of these you could detect homology, but a tree such as the one present in Figure 2, would help you understand the type of homologous relationship (i.e., whether it was orthology or paralogy).
#
# * https://en.wikipedia.org/wiki/Globin
# * https://en.wikipedia.org/wiki/Myoglobin
# * https://en.wikipedia.org/wiki/Hemoglobin
#
# <img src="./images/12862_2005_Article_216_Fig5_HTML.jpg" width=50%>
# Figure 2: A tree representing members of the globin gene family from diverse taxa.
# http://bmcevolbiol.biomedcentral.com/articles/10.1186/1471-2148-6-31
# A phylogenomic profile of globins by Vinogradov et al (2006).
#
# * Phylogenetic trees are used for many other diverse applications in bioinformatics.
# * It is therefore important that a bioinformatician have an understanding of how they are built, and how they should be interpreted.
# * An additional application that we'll cover in this text is comparing the composition of communities of organisms, but we'll come back to that later.

# %% [markdown]
# ## How phylogenies are reconstructed
# * Phylogenies are reconstructed using a variety of different algorithms, some of which we'll cover in this chapter.
# * These algorithms all work by comparing a set of *features* of organisms, and inferring the evolutionary distance between those organisms based on the similarity of their features.
# * The features that are compared can be nearly anything that is observable, either from extant organisms, or fossilized representatives of extinct organisms.
# * As an example, let's consider the reconstruction of the phylogeny of spiders (the order Araneae), a hypothesis of which is presented in Figure 5.
# * Of the extant spiders, some are orb-weavers (meaning they spin circular, sticky webs), and others are not.
#
# Take, for example, this fascinating creature:
# ![](./images/Orb-Weaver.jpg)
#
# * Entomologists have debated whether orb-weaving is either a:
#     * **polyphyletic** (that it evolved multiple times)
#         * Example: flight, which has evolved independently in birds, flying dinosaurs, insects, and mammals).
#     * **monophyletic trait** (that it evolved one time)
#         * Example: feathers, which evolved in birds, but not other flying creatures
# * If orb-weaving is monophyletic, it would mean that over the course of evolution, extant spiders which don't weave orb webs, had lost that ability at some point in the past.
#     * Some researchers doubt this, as it's a very effective means of catching prey, and losing that ability would likely constitute an evolutionary disadvantage.
# * If orb-weaving is polyphyletic, it would means that in at least two different spider lineages, this trait arose independently.
#     * Other researchers consider to be very unlikely due to the complexity of engineering these webs.
#
# Examples of the evolution of monophyletic and polyphyletic traits are presented in Figures 3 and 4, respectively.

# %% [markdown]
# <img src="./images/tree-monophyly.png">
# Figure 3: Example phylogeny illustrating a monophyletic trait shared by a group of organisms. In a monophyletic group, the last common ancestor was also member of the group (e.g., multicellular organisms).

# %% [markdown]
# <figure>
#     <img src="./images/tree-polyphyly.png">
#     <figcaption><b>Figure 4</b>: Example phylogeny illustrating a polyphyletic trait shared by a group of organisms. In a polyphyletic group the last common ancestor was not a member of the group (e.g., flying animals).</figcaption>
# </figure>

# %% [markdown]
# #### ++++++++++++++++ Cahoot-10-1
# https://umsystem.instructure.com/courses/97521/quizzes/290176

# %% [markdown]
# ## Inferring lineages
# Using:
# 1. morphology
# 2. sequence data

# %% [markdown]
# ### 1. Using morphological structure to infer lineages
#
# ![](./images/hdn.jpg)
# ![](./images/hdn-tree.jpg)
#
# And, back to spiders:
# * Earlier work on understanding the relations between the spider lineages focused on comparing traits that entomologists would observe, for example by watching spiders in action or by dissecting them.
# * For example, in 1986 through 1991, [Johnathan Coddington](http://entomology.si.edu/StaffPages/coddington.html) published several studies that tabulated and compared about sixty features of 32 spider taxa
#     * Coddington J. 1986. The monophyletic origin of the orb web. In: Shear W, ed. Spiders: webs, behavior, and evolution. Stanford, California: Stanford University Press. 319-363;
#     * Coddington JA. 1991. Cladistics and spider classification: araneomorph phylogeny and the monophyly of orbweavers (Araneae: Araneomorphae; Orbiculariae) Acta Zoologica Fennica 190:75-87).
#     * Features included whether
#         * a spider wraps its prey when it attacks (a behavioral trait), and
#         * how branched the spider's trachea is (a morphological trait).
#     * By determining which spiders were more similar and different across these traits, we provided early evidence for the hypothesis that orb-weaving is an ancient monophyletic trait.

# %% [markdown]
# ### 2. Using genetics to infer lineages
#
# For example, considering snake venom amino acid sequences:
# ![](./images/aminergic-toxins.png)
# https://www.nature.com/articles/s41598-017-02953-0
# https://pubmed.ncbi.nlm.nih.gov/28578406/
#
# And, back to spiders:
# * More recently, several research teams have used features of spider genomes to reconstruct the spider phylogeny (<a href="http://www.cell.com/current-biology/abstract/S0960-9822(14)00750-7">Bond et al., 2014</a>, [Garrison et al., 2016](https://peerj.com/articles/1719/)).
# * Using this approach, the features become the nucleotides observed at particular positions in the genome.
# * First we sequence specific genes that the researchers target that are present in all members of the group.
# * Then align those sequences with multiple sequence alignment.
# * This has several advantages over feature matrices derived from morphological and behavioral traits, including that many more features can be observed.
# * For example, ([Garrison et al., 2016](https://peerj.com/articles/1719/)), compared approximately 700,000 amino acid positions from nearly 4000 loci around the genomes of 70 spider taxa.
# * Compare the number of features here to the number mentioned in the previous paragraph.
# * These *phylogenomic* studies have further supported the idea that orb-weaving is an ancient monophyletic trait, and have provided much finer scale information on the evolution of spiders.
# * Supported by these data, researchers hypothesize that the loss of orb-weaving might not be that surprising.
# * While it does provide an effective means of catching flying insects, many insects which are potential prey for spiders don't fly.
# * Further, orb webs may attract predators of spiders, as they are easily observable signals of where a spider can be found.

# %% [markdown]
# ![](./images/spider-tree.png)
# Figure 5: A spider phylogeny.
# Numbers at internal nodes correspond to the taxonomic groups described in
# https://peerj.com/articles/1719/
# Garrison et al., 2016.
# https://doi.org/10.7717/peerj.1719/fig-1

# %% [markdown]
# For the remainder of this chapter, we'll consider methods for phylogenetic reconstruction that use genome sequence data as features.
#
# ## Some terminology
# Next, let's cover a few terms using the tree diagram in Figure 6.
#
# ![](./images/tree-schematic1.png)
# Figure 6: A schematic of a phylogenetic tree illustrating important terms.
#
# * *Terminal nodes or tips* typically represent extant organisms, also frequently called operational taxonomic units or OTUs.
# * OTU is a generic way of referring to a grouping of organisms (such as a species, a genus, or a phylum), without specifically identifying what that grouping is.
# * *Internal nodes* in a phylogenetic tree represent hypothetical ancestors.
# * We postulate their existence but often don't have direct evidence.
# * The *root node* is the internal node from which all other nodes in the tree descend.
# * This is often referred to as the *last common ancestor (LCA)* of the OTUs represented in the tree.
# * In a universal tree of life, the LCA is often referred to as *LUCA*, the *last universal common ancestor*.
# * All nodes in the tree can be referred to as OTUs.
# * *Branches* connect the nodes in the tree, and generally represent time or some amount of evolutionary change between the OTUs.
# * The specific meaning of the branches will depend on the method that was used to build the phylogenetic tree.
# * A *clade* in a tree refers to some node (either internal or terminal) and all nodes descending from it (i.e., moving away from the root toward the tips).
# * We'll use all of these terms below as we begin to explore phylogenetic trees.

# %% [markdown]
# ## Simulating evolution
# Something like this is the first thing I ever programmed just for fun, without it being as assignment...
# ![](./images/dna-sim.jpg)
#
# * Before we jump into how to reconstruct a phylogeny from DNA sequence data, we're going to perform a simulation of the process of evolution of a DNA sequence.
#     * We're going to model sequence evolution with a Python function
#     * Then we're going to run that function to simulate multiple generations of evolution.
#
# * Bioinformatics developers often use simulations to understand how their algorithms work, as they uniquely provide an opportunity to know what the correct answer is.
#     * This provides a way to compare algorithms to each other to figure out which performs best under which circumstances.
#
# * We're going to have control over the starting sequence, and the probability of incurring a substitution mutation or an insertion/deletion mutation at each position of the sequence in each generation.
#     * This would, for example, let us understand whether different algorithms for phylogenetic reconstruction are better or worse for more closely or distantly related sequences.
#
# Our simulation will works as follows.
# * We'll have one function that we primarily interact with called ``evolve_generations``, that will take:
#     * a starting sequence
#     * the number of generations that we want to simulate.
#     * the probability that we want a substitution mutation to occur at each position in the starting sequence, and
#     * the probability that we want either an insertion or deletion mutation to occur at each position.
# * In each generation, every sequence will spawn two new sequences, randomly incurring mutations at the prescribed rates.
# * This effectively simulates a clonal process of reproduction, a form of asexual reproduction common in single cellular organisms, where a parent cell divides into two cells, each containing a copy of the parent's genome with some errors introduced.
# * Let's inspect this code and then run our simulation beginning with a random sequence.

# %%
import numpy as np
import seaborn as sns  # type: ignore
import random
import skbio  # type: ignore
import functools
import ete3  # type: ignore
import scipy as sp  # type: ignore
from scipy import cluster
from typing import Callable

from IPython.core import page  # type: ignore

page.page = print


# %% [markdown]
# * First we'll look at the function used to simulate the evolution of a single sequence.
# * This is where much of what we're concerned about for sequence evolution happens.
# * This takes one sequence, and mutates it.

# %%
def evolve_sequence(
    sequence: skbio.Sequence, substitution_probability: float, indel_probability: float
) -> skbio.Sequence:
    sequence_length = len(sequence)
    insertion_choices = list(sequence.nondegenerate_chars)
    result: list[str] = []
    i = 0
    while i < sequence_length:
        current_char = sequence[i]
        if random.random() < substitution_probability:
            # simulate a substitution event by adding a character other than the current
            # character to the result at this position
            substituted_base = random.choice(
                seq=[r for r in sequence.nondegenerate_chars if r != current_char]
            )
            result.append(substituted_base)
            i += 1
        elif random.random() < indel_probability:
            # simulate either an insertion or a deletion event. the length of the insertion or
            # deletion is determined at random, with shorter lengths being more probable
            length = int(np.random.triangular(1, 1, 10))
            if np.random.binomial(1, 0.5) == 0:
                # simulate an insertion by adding length random characters from
                # this sequence's alphabet
                result.extend(np.random.choice(a=insertion_choices, size=length))
                i += 1
            else:
                # simulate a deletion by not appending any of the next length
                # characters
                i += length
        else:
            # simulate no mutation occurring
            result.append(str(current_char))
            i += 1

    return sequence.__class__("".join(result))


# %%
# Example
mutated_seq = evolve_sequence(
    sequence=skbio.DNA(sequence="ACTG"),
    substitution_probability=0.1,
    indel_probability=0.1,
)

print("Type:\n\t", type(mutated_seq))
print("Object:\n", mutated_seq)
mutated_seq


# %% [markdown]
# * Next, take a look at the function that models a single generation of a single sequence.
# * This is where the clonal reproduction (i.e., one parent sequence becoming two child sequences) occurs.
# * It takes one sequence, and produces two child sequences (mutated clones of the parent).

# %%
def evolve_generation(
    sequence: skbio.Sequence,
    substitution_probability: float,
    indel_probability: float,
    increased_rate_probability: float,
    fold_rate_increase: int,
) -> tuple[skbio.Sequence, skbio.Sequence]:
    child1 = evolve_sequence(
        sequence=sequence,
        substitution_probability=substitution_probability,
        indel_probability=indel_probability,
    )
    if random.random() < increased_rate_probability:
        child2 = evolve_sequence(
            sequence=sequence,
            substitution_probability=fold_rate_increase * substitution_probability,
            indel_probability=indel_probability,
        )
    else:
        child2 = evolve_sequence(
            sequence=sequence,
            substitution_probability=substitution_probability,
            indel_probability=indel_probability,
        )
    return child1, child2


# %%
# Example
next_gen = evolve_generation(
    sequence=skbio.DNA(sequence="ACTG"),
    substitution_probability=0.2,
    indel_probability=0.2,
    increased_rate_probability=0.0,
    fold_rate_increase=5,
)

print("Type of container:\n\t", type(next_gen))
print("Length of container:\n\t", len(next_gen))
print("Type of element:\n\t", type(next_gen[0]))
print("Object:\n", next_gen)
next_gen


# %% [markdown]
# * Finally, take a look at our main driver function.
# * This is where we provide the parameters of our simulation, including the starting sequence, the number of generations, and the mutation probabilities.
# * Notice how each of these functions builds on the prior functions.
# * This function starts with one sequence, and calls the above two sequences, to produce a data structure containing many generations of clonal reproduction.

# %%
def evolve_generations(
    ancestral_sequence: skbio.Sequence,
    generations: int,
    substitution_probability: float,
    indel_probability: float,
    increased_rate_probability: float = 0.0,
    fold_rate_increase: int = 5,
    verbose: bool = False,
) -> list[skbio.Sequence]:
    """
    ancestral_sequence : skbio.Sequence object
        The sequence that should be used as the root node in the tree.
    generations : int
        The number of generations to simulate. Must be greater than zero.
    substitution_probability : float
        The probability at which a substitution mutation should occur.
    indel_probability : float
        The probability at which either an insertion or a deletion mutation
        should occur. One of these will be simulated with even probability,
        and the length will be randomly chosen between 1 and 9, with shorter
        lengths being more probable.
    increased_rate_probability : float, optional
        The probability at which child2 should experience an increased rate
        of substitution mutations. Default is that this never occurs.
    fold_rate_increase : int, optional
        If increased_rate_probability is greater than zero, the fold increase
        that substitutions should now occur at.
    verbose : bool, optional
        If True, print the sequences that are simulated at each generation.

    """
    # initial some values and perform some basic error checking
    assert generations > 0, "Must simulate one or more generations."

    # initialize a list of the previous generations sequences - this gets used
    # in the for loop below. since we'll start with the first generation of
    # children, root_sequence is the previous generation's sequence
    ancestral_sequence.metadata["id"] = "0"
    previous_generation_sequences = [ancestral_sequence]

    # iterate for each requested generation
    for i in range(generations):
        # print the generation number and the current number of sequences
        if verbose:
            print("=====================================================")
            print(f"Generation: {i} (Number of parent sequences: {2**i})")
            print(f"{ancestral_sequence} (last common ancestor)\n")

        # create a list to store the current generation of sequences
        current_generation_sequences = []

        # iterate over the sequences of the previous generation
        for parent_sequence in previous_generation_sequences:
            # evolve two child sequences
            # currently the mutation probabilities are constant,
            # but should update that to change with generations
            r1, r2 = evolve_generation(
                sequence=parent_sequence,
                substitution_probability=substitution_probability,
                indel_probability=indel_probability,
                increased_rate_probability=increased_rate_probability,
                fold_rate_increase=fold_rate_increase,
            )
            r1.metadata["id"] = parent_sequence.metadata["id"] + ".1"
            r2.metadata["id"] = parent_sequence.metadata["id"] + ".2"
            current_generation_sequences.extend([r1, r2])
            if verbose:
                # if the caller specified verbose output, print the actual sequences
                print(
                    f"{parent_sequence} (parent id: {parent_sequence.metadata['id']})"
                )
                print(f"{r1} (child 1 id: {r1.metadata['id']})")
                print(f"{r2} (child 2 id: {r2.metadata['id']})\n")
        # current_generation_sequences becomes the next generation's
        # previous_generation_sequences
        previous_generation_sequences = current_generation_sequences

    # upon completion of all generations, return the last generation's sequences
    return previous_generation_sequences


# %%
# Example run
family_tree = evolve_generations(
    ancestral_sequence=skbio.DNA(sequence="ACTG"),
    generations=3,
    substitution_probability=0.1,
    indel_probability=0.05,
    increased_rate_probability=0.1,
    verbose=True,
)

# %%
print("Type of container:\n\t", type(family_tree))
print("Length of container:\n\t", len(family_tree))
print("Type of element:\n\t", type(family_tree[0]))
print("Object:\n", family_tree)
family_tree


# %% [markdown]
# #### ++++++++++++++++++ Cahoot-10-2
# https://umsystem.instructure.com/courses/97521/quizzes/290177

# %% [markdown]
# ## Now we'll run our simulation.
# * We'll start with a random DNA sequence, and then evolve three generations.
# * Before running this, can you predict how many child sequences we'll end up with after three generations?
#
# * When we call ``evolve_generations``, we'll pass the parameter ``verbose=True``.
# * This will tell the function to print out some information throughout the process.
# * This will let us inspect our evolutionary process: something that is impossible to do with real sequences from natural history.
#     * At least we could observe microbial evolution in the lab, but that's still a lot harder than this computational test of our methods.

# %%
def random_sequence(moltype: skbio.Sequence, length: int) -> skbio.Sequence:
    result = []
    alphabet = list(moltype.nondegenerate_chars)
    for e in range(length):
        result.append(random.choice(seq=alphabet))
    return moltype("".join(result))


# %%
sequence = random_sequence(moltype=skbio.DNA, length=50)

sequences = evolve_generations(
    ancestral_sequence=sequence,
    generations=3,
    substitution_probability=0.1,
    indel_probability=0.05,
    increased_rate_probability=0.1,
    verbose=True,
)

# %% [markdown]
# * We now have a new variable, sequences, which contains the child sequences from the last generation.
# * Take a minute to look at the ids of the parent and child sequences above, and the ids of a couple of the final generation sequences.
# * These ids are constructed so that each sequence contains the identifiers of its ancestral sequences, and then either ``1`` or ``2``.
# * Notice that all sequence identifiers start with ``0``, the identifier of the last common ancestor (or our starting sequence) of all of the sequences.
# * These identifiers will help us interpret whether the phylogenies that we reconstruct accurately represent the evolutionary relationships between the sequences.
# * Also, notice that at this point we only have the sequences from the last generation.
# * We no longer have the ancestral sequences (which would correspond to the internal nodes in the tree).
# * This models the real world, were we only have sequences from extant organisms, but not their ancestors.
# * Take a minute to compare the two sequences below. What types of mutations happened over the course of their evolution?

# %%
print(len(sequences))

# %%
print(sequences[0])
sequences[0]

# %%
print(sequences[-1])
sequences[-1]


# %% [markdown]
# * In our simulation, each sequence is directly derived from exactly one sequence from the previous generation, and the evolution of all of the sequences traces back to starting sequence that we provided.
# * This means that our final sequences are all homologous.
# * And because we have modeled this process, we know where each sequence fits in relation to all of the other sequences in the phylogeny.
# * Our goal with the algorithms we'll study for the rest of this chapter is to reconstruct that phylogeny given only the last generation of sequences.
# * We'll use the fact that we know the true phylogeny to help us evaluate the relative performance of the different methods.
#
# <figure>
#     <img src="./images/sequence-evo-tree.png">
#     <figcaption><b>Figure 7</b>: Schematic of a simulated evolutionary process. Bases in red indicate mutation since the last common ancestor. The bottom panel illustrates the real-world equivalent of our final product, where we wouldn't know the true phylogeny (indicated by the dashed branches), the sequence of the last common ancestor, or what positions have changed since the last common ancestor.</figcaption>
# </figure>
#
# * Let's simulate 10 generations of sequences here, and then randomly select some of those sequences to work with in the remaining sections of this chapter.
# * For the sake of runtime, I'm going to set our ``indel_probability=0.0``.
# * This means that we won't need to align our selected sequences before constructing trees from them (because with only substitution mutations occurring, homologous positions always remain aligned).
# * Multiple sequence alignment is currently a runtime bottleneck, so this will let us run this notebook much faster.
# * If you'd like to model insertion/deletions, you can increase the ``indel_probability``, say to ``0.005``.
# * If you do that, the sequences will be aligned for you in the next cell, but it may take around 30 minutes to run.

# %%
# Recall from previous section:
def kmer_distance(
    sequence1: skbio.Sequence,
    sequence2: skbio.Sequence,
    k: int = 3,
    overlap: bool = True,
) -> float:
    """Compute the kmer distance between a pair of sequences

    Parameters
    ----------
    sequence1 : skbio.Sequence
    sequence2 : skbio.Sequence
    k : int, optional
        The word length.
    overlapping : bool, optional
        Defines whether the k-words should be overlapping or not
        overlapping.

    Returns
    -------
    float
        Fraction of the set of k-mers from both sequence1 and
        sequence2 that are unique to either sequence1 or
        sequence2.

    Raises
    ------
    ValueError
        If k < 1.

    Notes
    -----
    k-mer counts are not incorporated in this distance metric.

    """
    sequence1_kmers = set(map(str, sequence1.iter_kmers(k=k, overlap=overlap)))
    sequence2_kmers = set(map(str, sequence2.iter_kmers(k=k, overlap=overlap)))
    all_kmers = sequence1_kmers | sequence2_kmers
    shared_kmers = sequence1_kmers & sequence2_kmers
    number_unique = len(all_kmers) - len(shared_kmers)
    fraction_unique = number_unique / len(all_kmers)
    return fraction_unique


# %%
# Recall from previous section:
def progressive_msa(
    sequences: list[skbio.Sequence],
    pairwise_aligner: Callable[[skbio.Sequence, skbio.Sequence], skbio.TabularMSA],
    guide_tree: skbio.TreeNode = None,
    metric: Callable[
        [skbio.Sequence, skbio.Sequence, int, bool], float
    ] = kmer_distance,
) -> skbio.TabularMSA:
    """Perform progressive msa of sequences

    Parameters
    ----------
    sequences : skbio.SequenceCollection
        The sequences to be aligned.
    metric : function, optional
      Function that returns a single distance value when given a pair of
      skbio.Sequence objects. This will be used to build a guide tree if one
      is not provided.
    guide_tree : skbio.TreeNode, optional
        The tree that should be used to guide the alignment process.
    pairwise_aligner : function
        Function that should be used to perform the pairwise alignments,
        for example skbio.alignment.global_pairwise_align_nucleotide. Must
        support skbio.Sequence objects or skbio.TabularMSA objects
        as input.

    Returns
    -------
    skbio.TabularMSA
    """

    if guide_tree is None:
        guide_dm = skbio.DistanceMatrix.from_iterable(
            iterable=sequences, metric=metric, key="id"
        )
        guide_lm = sp.cluster.hierarchy.average(y=guide_dm.condensed_form())
        guide_tree = skbio.TreeNode.from_linkage_matrix(
            linkage_matrix=guide_lm, id_list=guide_dm.ids
        )

    seq_lookup = {s.metadata["id"]: s for i, s in enumerate(sequences)}

    # working our way down, first children may be super-nodes,
    # then eventually, they'll be leaves
    c1, c2 = guide_tree.children

    # Recursive base case
    if c1.is_tip():
        c1_aln = seq_lookup[c1.name]
    else:
        c1_aln = progressive_msa(
            sequences=sequences, pairwise_aligner=pairwise_aligner, guide_tree=c1
        )

    if c2.is_tip():
        c2_aln = seq_lookup[c2.name]
    else:
        c2_aln = progressive_msa(
            sequences=sequences, pairwise_aligner=pairwise_aligner, guide_tree=c2
        )

    # working our way up, doing alignments, from the bottom up
    alignment, _, _ = pairwise_aligner(seq1=c1_aln, seq2=c2_aln)

    # this is a temporary hack as the aligners in skbio 0.4.1 are dropping
    # metadata - this makes sure that the right metadata is associated with
    # the sequence after alignment
    if isinstance(c1_aln, skbio.Sequence):
        alignment[0].metadata = c1_aln.metadata
        len_c1_aln = 1
    else:
        for i in range(len(c1_aln)):
            alignment[i].metadata = c1_aln[i].metadata
        len_c1_aln = len(c1_aln)
    if isinstance(c2_aln, skbio.Sequence):
        alignment[1].metadata = c2_aln.metadata
    else:
        for i in range(len(c2_aln)):
            alignment[len_c1_aln + i].metadata = c2_aln[i].metadata

    # feed alignment back up, for further aligment, or eventually final return
    return alignment


# %%
indel_probability = 0.0

sequences = evolve_generations(
    ancestral_sequence=sequence,
    generations=10,
    substitution_probability=0.03,
    indel_probability=0.0,
    increased_rate_probability=0.1,
    verbose=False,
)

sequences = random.sample(population=sequences, k=25)

# Test for need to do alignment (indels occured)
if abs(0 - indel_probability) < 1e-08:
    sequences_aligned = sequences
else:
    gpa = functools.partial(
        skbio.alignment.global_pairwise_align_nucleotide, penalize_terminal_gaps=True
    )
    sequences_aligned = progressive_msa(sequences=sequences, pairwise_aligner=gpa)

# %% [markdown]
# ### Simulations are limited
# ![](./images/simulation-bacteria.jpg)
#
# * While simulations can be helpful for comparing algorithms, they can also be mis-interpreted easily.
# * This is, in part, because when we model evolution, we simplify the evolutionary process, missing variables and assumptions.
# * For example, one assumption that our simulation is making is that "bursts" of evolution (i.e., where our substitution rate temporarily increases) are restricted to only a single generation.
# * A child is no more or less like to have an increased rate of substitutions if its parent did.
# * This may, or may not, be an accurate model.
# * Imagine in the real-world that the environment changed drastically for some descendants
#     * For example, if a geological event created new thermal vents in a lake they inhabited, resulting in an increase in mean water temperature, but not for others.
#     * The lineages who experience the environmental change might have an increased rate of substitutions as their genomes adapt to the new environment.
#     * An increased substitution rate may persist for multiple generations, or it may not.
# * When using simulations, it's important to understand what assumptions the simulation makes, so you know what it can tell you about, and what it can't tell you about.
# * You'll want to consider your degree of confidence in the results of an evaluation, based on any simulation.
#
# #### Why question:
# What are some other assumptions that are being made by the evolutionary simulation presented here?
# There are many, so take a minute to list a few.

# %% [markdown]
# ### Real experiments are limited
#
# ![](./images/last-common.jpg)
#
# For real extant organisms, previous generations are not available.
#
# * On the opposite end of the spectrum from simulations for algorithm comparison is comparisons based on real data, where we don't know ground truth.
# * The trade-off is that, while there are no assumptions being made in real data with respect to the composition of the sequences, with real data we don't usually know what the right answer (in our case, the correct phylogeny) is (unless we record it in the a microbiology lab), so it's harder to determine which algorithms are doing better or worse.
# * The take-away message here is that neither approach is perfect, and often researchers will use a combination of simulated and real data to evaluate algorithms.

# %% [markdown]
# ## Visualizing trees with ete3
# * As we now start computing phylogenetic trees, we're going to need a way to visualize them.
# * We'll use the ete3 Python package for this, and in the next cell we'll configure the ``TreeStyle`` which is used to define how the trees we visualize will look.
# * If you'd like to experiment with other views, you can modify the code in this cell according to the [ete3 documentation](http://etetoolkit.org/docs/latest/tutorial/tutorial_drawing.html).
# * If you come up with a nicer style, I'd be interested in seeing that.

# %%
help(ete3.Tree)

# %%
ts = ete3.TreeStyle()
ts.show_leaf_name = True
ts.scale = 250
ts.branch_vertical_margin = 15

# %% [markdown]
# * We can apply this ``TreeStyle`` to a random tree as follows.
# * Any changes that you make to the ``TreeStyle`` above will impact this tree and all following trees in this chapter.
# * Experiment with this - it's fun!

# %%
# Make a tree object
t = ete3.Tree()

# Generates a random tree of 10 leaf nodes
t.populate(size=10)

# Plot the tree
t.render(file_name="%%inline", tree_style=ts)

# %% [markdown]
# ## Distance-based approaches of phylogenetic reconstruction
# * The next approaches we'll take for phylogenetic reconstruction rely on computing distances between sequences.
# * We've previously discussed distances between sequences in a few places in the text.
# * We'll begin this section by formalizing the term *distance*, and introducing the concept of a distance matrix.
#
# ### Distances and distance matrices
# * Formally, a *distance* between a pair of objects is a measure of their dissimilarity.
# * There isn't one single definition of the distance between two objects.
# * For example, if your two objects are the cities Flagstaff, Arizona and Boulder, Colorado you might measure the distance between them as length of shortest line that connects them.
# * This would be the *Euclidean distance* between the two cities.
# * If you're trying to travel from one city to another however, that might not be the most relevant distance measure.
# * Instead, you might be interested in something like their *Manhatten distance* (taxicab distance), which in this would be more similar to a measure of the shortest route that you could travel between the two cities using the Interstate Highway system.
# * You can see how this is different than the shortest line connecting the two cities.
#
# ![image6.png](./images/image6.png)
#
# An illustration comparing the taxicab metric to the Euclidean metric on the plane:
# * According to the taxicab metric all three pictured paths (red, yellow, and blue) have the same length (12).
# * According to the Euclidean metric, the green path has length and the unique shortest path is $$6\sqrt{2} = 8.49$$
#
# Similarly, different distance metrics will be relevant or not, for different types of objects.
# * Clearly the way you measure distances between cities is very different than the way we measure distances between biological sequences.
# * However, the abstract concept of a distance between two objects is the same.
#
# https://en.wikipedia.org/wiki/Metric_(mathematics)
#
# https://en.wikipedia.org/wiki/Metric_space
#
# * In mathematics, a metric or distance function is a function that defines a distance between each pair of elements of a set.
# * A set with a metric is called a metric space.

# %% [markdown]
# ### Similarity
# https://en.wikipedia.org/wiki/Similarity_measure
#
# * In statistics and related fields, a similarity measure or similarity function is a real-valued function that quantifies the similarity between two objects.
# * Although no single definition of a similarity measure exists, usually such measures are in some sense the inverse of distance metrics:
#     * they take on large values for similar objects and either zero or a negative value for very dissimilar objects.

# %% [markdown]
# Formally, a measure of dissimilarity $d$ between two objects $x$ and $y$ is a *distance* if it meets these four criteria for all $x$ and $y$:
# 1. $d(x,y) \geq 0$ (non-negativity)
# 2. $d(x,y) = 0\ if\ and\ only\ if\ x = y$ (identity of indiscernibles)
# 3. $d(x,y) = d(y,x)$ (symmetry)
# 4. $d(x,z) \leq d(x,y) + d(y,z)$ (triangle inequality)
#
# When we compute the distances between some number of objects $n$, we'll commonly represent those values in a *distance matrix* which contains all of those values
#
# ![image7.png](./images/image7.png)
#
# * Note the tree on the axes.
# * We could have generated something like this from the matrix data itself.
# * Then, we can use it to sort the matrix data internally, illustrating clusters in the heatmap colors.
#
# Distance matrices are so common in bioinformatics that scikit-bio defines a [DistanceMatrix object](http://scikit-bio.org/docs/latest/generated/generated/skbio.stats.distance.DistanceMatrix.html).
# It provides a convenient interface for working with these data.

# %%
help(skbio.DistanceMatrix)

# %%
help(skbio.stats.distance._base.DissimilarityMatrix)

# %% [markdown]
# We can create one of these objects as follows:

# %%
dm = skbio.DistanceMatrix(
    data=[[0.0, 1.0, 2.0], [1.0, 0.0, 3.0], [2.0, 3.0, 0.0]], ids=["a", "b", "c"]
)

# %% [markdown]
# We can then access the values in the distance matrix directly, view the distance matrix as a heatmap, and do many other things that facilitate analyzing distances between objects.

# %%
print(dm)
print(dm["a", "b"])
print(dm["b", "c"])
dm

# %%
help(dm.plot)

# %%
_ = dm.plot(cmap="Greens")

# %% [markdown]
# * The conditions of a distance metric listed above lead to a few specific features of distance matrices:
#     * they're *symmetric* (if you flip the upper triangle over the diagonal, then the values are the same as those in the lower triangle),
#     * *hollow* (the diagonal is all zeros), and
#     * all values are greater than or equal to zero.
# * Which of the conditions listed above results in each of these features of distance matrices?
#
# ## Distances between sequences
# Can be categorized into those that:
# 1. don't require alignment
# 2. require alignment

# %% [markdown]
# ### 1. Alignment-free distances between sequences
# * We've now looked at several ways of computing distances between sequences, some of which have required that the positions in the sequences are directly comparable to one another (i.e., that our sequences are aligned), and some of which haven't.
# * One *alignment-free* distance between sequences is the k-mer distance that we worked with in *Sequence Homology Searching*
# * We can use the `kmer_distance` function with scikit-bio as follows to create an ``skbio.DistanceMatrix`` object.
# * These ``skbio.DistanceMatrix`` objects can be viewed as heatmaps.
# * When interpreting these heatmaps, be sure to note the scale on the color bar.
# * Since the distance metrics we compare here differ in their maximum values, the scale differs for each of these such that the darkest color can represent a different value in each heatmap.

# %%
# Recall from before
# %psource kmer_distance

# %%
help(skbio.DistanceMatrix.from_iterable)

# %psource skbio.DistanceMatrix.from_iterable

# %%
kmer_dm = skbio.DistanceMatrix.from_iterable(
    iterable=sequences, metric=kmer_distance, key="id"
)
_ = kmer_dm.plot(cmap="Greens", title="3mer distances between sequences")

# %% [markdown]
# ### 2. Alignment-based distances between sequences
# * One alignment-based distance metric that we've looked at is Hamming distance.
# * This would be considered an alignment-based approach because is does consider the order of the characters in the sequence by comparing a character at a position in one sequence only to the character at the corresponding position in the other sequence.
# * We could compute these distances as follows, after first aligning our sequences.
#
# Note: here, we specified a 0 probability of indel mutations, with sequences of identical length, and thus they are already aligned:

# %%
hamming_dm = skbio.DistanceMatrix.from_iterable(
    iterable=sequences_aligned, metric=skbio.sequence.distance.hamming, key="id"
)
_ = hamming_dm.plot(cmap="Greens", title="Hamming distances between sequences")


# %% [markdown]
# ### Jukes-Cantor correction of observed distances between sequences
#
# Why question: How long should branches be? Does it matter for our conclusions?
#
# * UPGMA is "ultrametric", meaning that all the terminal nodes (i.e. the sequences/taxa) are equally distance from the root.
# * In molecular terms, this means that UPGMA assumes a molecular clock, i.e. all lineages are evolving at a constant rate.
# * In practical terms, this means that you can construct a distance scale bar and all the terminal nodes will be level at position 0.0, representing the present.
# * In this example, the scale bar is shown on the right-hand side.
#
# ![image8.png](./images/image8.png)
#
# * The Hamming distance between aligned sequences, as described above, is simple to calculate, but it is often an underestimate of the actual amount of mutation that has occurred in a sequence.
# * Here's why: imagine that in one generation $g$, position $p$ of sequence $S1$ undergoes a substitution mutation from ``A`` to ``C``. Then, in the next generation $g + 1$, the same position $p$ of sequence $S1$ undergoes a substitution from ``C`` to ``T``.
# * Because we can only inspect the modern-day sequences, not their ancestors, it looks like position $p$ has had a single substitution event.
# * Similarly, if in generation $g + 1$ position $p$ changed from ``C`` back to ``A`` (a *back substitution*), we would observe zero substitution mutations at that position even though two had occurred.
#
# * To correct for this, the *Jukes-Cantor correction* is typically applied to the Hamming distances between the sequences.
# * Where $p$ is the Hamming distance, the corrected genetic distance is computed as $d = -\frac{3}{4} \ln(1 - \frac{4}{3}p)$.
# * The derivation of this formula is beyond the scope of this text (you can find it in Inferring Phylogeny by Felsenstein), but it is based on the Jukes-Cantor (JC69) nucleotide substitution model.
#
# https://en.wikipedia.org/wiki/Substitution_model
#
# * The Python implementation of this correction looks like the following.
# * We can apply this to a number of input distance values to understand how it transforms our Hamming distances.

# %%
def jc_correction(p: float) -> float:
    return (-3 / 4) * float(np.log(1 - (4 * p / 3)))


# %%
distances = np.arange(0.0, 0.70, 0.05)

jc_corrected_distances = list(map(jc_correction, distances))

ax = sns.pointplot(distances, jc_corrected_distances)

ax.set_xlabel("Hamming distance")
ax.set_ylabel("JC-corrected distance")
ax.set_xlim(0)
ax.set_ylim(0)

ax


# %% [markdown]
# We can then apply this to a full distance matrix as follows (we'll then print the first row of each).

# %%
def jc_correct_dm(dm: skbio.DistanceMatrix) -> skbio.DistanceMatrix:
    result = np.zeros(dm.shape)
    for i in range(dm.shape[0]):
        for j in range(i):
            result[i, j] = result[j, i] = jc_correction(p=dm[i, j])
    return skbio.DistanceMatrix(data=result, ids=dm.ids)


jc_corrected_hamming_dm = jc_correct_dm(dm=hamming_dm)

# %%
print(hamming_dm[0])
print(jc_corrected_hamming_dm[0])

# %%
# Pre-correction:
_ = hamming_dm.plot(cmap="Greens", title="Hamming distances between sequences")

# Post-correction:
_ = jc_corrected_hamming_dm.plot(
    cmap="Greens", title="JC-corrected Hamming distances between sequences"
)

# %% [markdown]
# ### Phylogenetic reconstruction with UPGMA
# The first algorithm we'll look at for reconstructing phylogenetic trees is called UPGMA, which stands for *Unweighted Pair-Group Method with Arithmetic mean*.
# * While that name sounds complex, it's actually a straightforward algorithm, which is why we're starting with it.
# * After we work through the algorithm, we'll come back to the name as it'll make more sense then.
#
# UPGMA is a generic hierarchical clustering algorithm.
# * It's not specific to reconstructing biological trees, but rather is used for interpreting any type of distance matrix.
# * It is fairly widely used for building phylogenetic trees, though it's application in phylogenetics is usually restricted to building preliminary trees to "guide" the process of multiple sequence alignment.
# * The reason for this is that it's fast, but it makes some assumptions that don't work well for inferring relationships between organisms, which we'll discuss after working through the algorithm.
#
# UPGMA starts with a distance matrix, and works through the following steps to create a tree.
# 1. **Step 1**: Find the smallest non-zero distance in the matrix and define a clade containing only those members.
#     * Draw that clade, and set the total length of the branch connecting the tips to the distance between the tips.
#     * The distance between each tip and the node connecting them should be half of the distance between the tips.
# 2. **Step 2**: Create a new distance matrix with an entry representing the new clade created in step 1.
# 3. **Step 3**: Calculate the distance matrix entries for the new clade as the mean distance from each of the tips of the new clade to all other tips in the *original* distance matrix.
# 4. **Step 4**: If there is only one distance (below or above the diagonal) in the distance matrix, use it to connect the remaining unconnected clades, and stop. Otherwise repeat step 1.
#
# Let's work through these steps for a simple distance matrix representing the distances between five sequences.

# %%
_data: np.ndarray = np.array(
    [
        [0.0, 4.0, 2.0, 5.0, 6.0],
        [4.0, 0.0, 3.0, 6.0, 5.0],
        [2.0, 3.0, 0.0, 3.0, 4.0],
        [5.0, 6.0, 3.0, 0.0, 1.0],
        [6.0, 5.0, 4.0, 1.0, 0.0],
    ]
)

_ids = ["s1", "s2", "s3", "s4", "s5"]

master_upgma_dm = skbio.DistanceMatrix(data=_data, ids=_ids)

print(master_upgma_dm)

# %% [markdown]
# Iteration 1
# ------------
#
# * Step 1: The smallest non-zero distance in the above matrix is between `s4` and `s5`.
# * So, we'll draw that clade and set each branch length to half of the distance between them.
#
# <img src="./images/upgma-tree-iter1.png">
#
# * Step 2: Next, we'll create a new, smaller distance matrix where the sequences `s4` and `s5` are now represented by a single clade which we'll call `(s4, s5)`.
# * This notation indicates that the corresponding distances are to both ``s4`` and ``s5``.

# %%
iter1_ids = ["s1", "s2", "s3", "(s4, s5)"]

# %% [markdown]
# * Step 3: We'll now fill in the None values from the new clade to each of the existing sequences (or clades).
# * The distance will be the mean between each pre-existing clade, and each of the sequences in the new clade.
# * For example, the distance between `s1` and `(s4, s5)` is the mean of the distance between `s1` and `s4` and `s1` and `s5`:

# %%
# Note: we are not using the iter1_dm, but the original matrix (master_upgma_dm)
s1_s4s5 = np.mean([master_upgma_dm["s1", "s4"], master_upgma_dm["s1", "s5"]])
print(s1_s4s5)

# %% [markdown]
# Similarly, the distance between `s2` and `(s4, s5)` is the mean of the distance between `s2` and `s4` and `s2` and `s5`:

# %%
s2_s4s5 = np.mean([master_upgma_dm["s2", "s4"], master_upgma_dm["s2", "s5"]])
print(s2_s4s5)

# %% [markdown]
# And finally, the distance between `s3` and `(s4, s5)` is the mean of the distance between `s3` and `s4` and the distance between `s3` and `s5`:

# %%
s3_s4s5 = np.mean([master_upgma_dm["s3", "s4"], master_upgma_dm["s3", "s5"]])
print(s3_s4s5)

# %% [markdown]
# * We can fill these values in to our iteration 1 distance matrix.
# * Why do we only need to compute three values to fill in seven cells in this distance matrix?

# %%
iter1_dm = [
    [0.0, 4.0, 2.0, s1_s4s5],
    [4.0, 0.0, 3.0, s2_s4s5],
    [2.0, 3.0, 0.0, s3_s4s5],
    [s1_s4s5, s2_s4s5, s3_s4s5, 0.0],
]

iter1_dm = skbio.DistanceMatrix(data=iter1_dm, ids=iter1_ids)

print(iter1_dm)

# %% [markdown]
# * Step 4: Because there is still more than one value below the diagonal in our new distance matrix, we start a new iteration by going back to Step 1 and repeating this process.

# %% [markdown]
# Iteration 2
# ------------
#
# * Step 1: The smallest non-zero distance in the iteration 1 distance matrix is between `s1` and `s3`.
# * So, we'll draw that clade and set each branch length to half of that distance.
#
# <img src="./images/upgma-tree-iter2.png">
#
# * Step 2: We next create a new, smaller distance matrix where the sequences `s1` and `s3` are now represented by a single clade, `(s1, s3)`.

# %%
iter2_ids = ["(s1, s3)", "s2", "(s4, s5)"]

# %% [markdown]
# * Step 3: We'll now fill in the values from the new clade to each of the existing sequences (or clades).
# * Notice that the distance between our new clade and ``s2`` is the mean of two values, but the distance between our new clade and the clade defined in iteration 1 is the mean of four values.
# * Why is that?

# %%
# For combining on s2 (index 1)
s2_s1s3 = np.mean([master_upgma_dm[1][0], master_upgma_dm[1][2]])

s4s5_s1s3 = np.mean(
    [
        master_upgma_dm[0][3],
        master_upgma_dm[0][4],
        master_upgma_dm[2][3],
        master_upgma_dm[2][4],
    ]
)

# %% [markdown]
# We can now fill in all of the distances in our iteration 2 distance matrix.

# %%
iter2_dm = [[0.0, s2_s1s3, s4s5_s1s3], [s2_s1s3, 0.0, 5.5], [s4s5_s1s3, 5.5, 0.0]]
iter2_dm = skbio.DistanceMatrix(data=iter2_dm, ids=iter2_ids)
print(iter2_dm)

# %% [markdown]
# * Step 4: There is still more than one value below the diagonal, so we start a new iteration by again repeating the process.
#
# Iteration 3
# ------------
#
# * Step 1: The smallest non-zero distance in the above matrix is now between `(s1, s3)` and `s2`.
# * So, we'll draw that clade and set each branch length to half of the distance.
#
# <img src="./images/upgma-tree-iter3.png">
#
# * Step 2: We'll next create a new distance matrix where the clade `(s1, s3)` and the sequence `s2` are now represented by a single clade, `((s1, s3), s2)`.

# %%
iter3_ids = ["((s1, s3), s2)", "(s4, s5)"]

# %% [markdown]
# * Step 3: We'll now fill in the values from the new clade to each of the existing sequences (or clades).
# * This is now the mean of six distances.
# * Why?

# %%
s1s2s3_s4s5 = np.mean(
    [
        master_upgma_dm[0][3],
        master_upgma_dm[0][4],
        master_upgma_dm[2][3],
        master_upgma_dm[2][4],
        master_upgma_dm[1][3],
        master_upgma_dm[1][4],
    ]
)

# %% [markdown]
# We fill this value into our iteration 3 distance matrix.

# %%
iter3_dm = [[0.0, s1s2s3_s4s5], [s1s2s3_s4s5, 0.0]]

iter3_dm = skbio.DistanceMatrix(data=iter3_dm, ids=iter3_ids)

print(iter3_dm)


# %% [markdown]
# * Step 4: At this stage, there is only one distance below the diagonal in our distance matrix.
# * So, we can use that distance to draw the final branch.
# * This will connect our two deepest clades, `((s1, s3), s2)` and `(s4, s5)`, which will give us our final UPGMA tree.
#
# <img src="./images/upgma-tree-final.png">
#
# #### Applying UPGMA from SciPy
# * [SciPy](http://www.scipy.org/) contains an implementation of UPGMA that we can apply to our existing distance matrices, and we can then visualize the resulting trees with ete3.
# * We provide a *wrapper function* that will give this an interface that is convenient to work with.

# %%
def tree_from_distance_matrix(dm: skbio.DistanceMatrix, metric: str) -> skbio.tree:
    if metric == "upgma":
        lm = sp.cluster.hierarchy.average(y=dm.condensed_form())
        tree = skbio.TreeNode.from_linkage_matrix(linkage_matrix=lm, id_list=dm.ids)
    elif metric == "nj":
        tree = skbio.tree.nj(dm=dm)
    else:
        raise ValueError(
            f"Unknown metric: {metric}. Supported metrics are 'upgma' and 'nj'."
        )
    return tree


# %%
def guide_tree_from_sequences(
    sequences: list[skbio.Sequence],
    metric: Callable[
        [skbio.Sequence, skbio.Sequence, int, bool], float
    ] = kmer_distance,
    display_tree: bool = False,
) -> sp.cluster.hierarchy.ClusterNode:
    """Build a UPGMA tree by applying metric to sequences

    Parameters
    ----------
    sequences : list of skbio.Sequence objects (or subclasses)
      The sequences to be represented in the resulting guide tree.
    metric : function
      Function that returns a single distance value when given a pair of
      skbio.Sequence objects.
    display_tree : bool, optional
      Print the tree before returning.

    Returns
    -------
    skbio.TreeNode

    """
    guide_dm = skbio.DistanceMatrix.from_iterable(
        iterable=sequences, metric=metric, key="id"
    )
    guide_lm = sp.cluster.hierarchy.average(y=guide_dm.condensed_form())
    guide_tree = sp.cluster.hierarchy.to_tree(Z=guide_lm)
    if display_tree:
        guide_d = sp.cluster.hierarchy.dendrogram(
            Z=guide_lm,
            labels=guide_dm.ids,
            orientation="right",
            link_color_func=lambda x: "black",
        )
    return guide_tree


# %%
help(sp.cluster.hierarchy.average)

# %% [markdown]
# * Let's compute and visualize UPGMA trees for the two distance matrices that we created above.
# * How do these trees compare to one another?
# * Does one look more or less correct than the other?
# * They may or may not, depending on the random sample of sequences that are being compared.
# * One thing to be aware of as you start visualizing trees is that the vertical order (in the default ``TreeStyle`` being used here) doesn't have biological meaning, it's purely a visualization component.
# * You can rotate the branches descending from any node in the tree freely.

# %%
# This is what we used in a previous section:
guide_tree_from_sequences(
    sequences=sequences,
    metric=kmer_distance,
    display_tree=True,
)

# %%
kmer_tree = tree_from_distance_matrix(dm=kmer_dm, metric="upgma")

print(kmer_tree)
print(type(kmer_tree))

# %%
ete3.Tree(newick=str(kmer_tree), format=1).render(file_name="%%inline", tree_style=ts)

# %%
jc_corrected_hamming_tree = tree_from_distance_matrix(
    dm=jc_correct_dm(dm=hamming_dm), metric="upgma"
)
ete3.Tree(newick=str(jc_corrected_hamming_tree), format=1).render(
    file_name="%%inline", tree_style=ts
)

# %% [markdown]
# * Which is better?
# * Which produces a higher percentage of agreement between leaf pairs?
# * How could we compute that?

# %% [markdown]
# #### Understanding the name
# * As mentioned above, UPGMA has a rather complex sounding name: *Unweighted Pair Group metric with Arithmetic mean*.
# * The *Unweighted* term indicates that all tip-to-tip distances contribute equally to each average that is computed (no weighted averages are being computed).
# * The *Pair Group* term implies that all internal nodes, including the root node, will be strictly bifurcating, or descent to exactly two other nodes (either internal or terminal).
# * The *Arithmetic mean* term implies that distances to each clade are the mean of distances to all members of that clade.

# %% [markdown]
# ### Phylogenetic reconstruction with neighbor-joining
# An alternative method:
# * One invalid assumption that is made by UPGMA is inherent in Step 1, where each branch connecting the internal node to a tip is set to half of the length between the tips.
# * This assumes the mutation rates are constant throughout the tree, or in other words that the tree is *ultrametric*.
# * This is not likely to be the case in the real world, as different lineages in the tree might be undergoing different selective pressures, leading to different rates of evolution.
# * Neighboring joining is a distance-based phylogenetic reconstruction approach that does not assume ultrametricity.
# *  Neighbor-joining methods also apply general data clustering techniques to sequence analysis using genetic distance as a clustering metric.
# * The simple neighbor-joining method produces unrooted trees, but it does not assume a constant rate of evolution (i.e., a molecular clock) across lineages.
# * http://scikit-bio.org/docs/latest/generated/skbio.tree.nj.html
# * https://en.wikipedia.org/wiki/Neighbor_joining
# * http://evolution.genetics.washington.edu/phylip/doc/neighbor.html
#
# ![image9.png](./images/image9.png)

# %%
nj_tree = tree_from_distance_matrix(dm=jc_correct_dm(dm=hamming_dm), metric="nj")

ete3.Tree(newick=str(nj_tree), format=1).render(file_name="%%inline", tree_style=ts)

# %% [markdown]
# * How many possible nj phylogenies are there for a given collection of sequences (where only branch pattern but not length varies)?

# %% [markdown]
# ## Rooted versus unrooted trees
#
# https://en.wikipedia.org/wiki/Phylogenetic_tree#Rooted_tree
#
# https://en.wikipedia.org/wiki/Tree_(graph_theory)#Rooted_tree

# %% [markdown]
# ### A rooted phylogenetic tree is a directed tree with a unique node, the root, corresponding to the (usually imputed) most recent common ancestor of all the entities at the leaves of the tree.
# * The root node does not have a parent node, but serves as the parent of all other nodes in the tree.
# * The root is therefore a node of degree 2 while other internal nodes have a minimum degree of 3 (where "degree" here refers to the total number of incoming and outgoing edges).
# * The most common method for rooting trees is the use of an uncontroversial outgroup close enough to allow inference from trait data or molecular sequencing, but far enough to be a clear outgroup.
#
# The following is a *rooted tree*, which means that it includes an assumption about the last common ancestor of all sequences represented in the tree.
#
# ![](./images/basic-rooted-tree1.jpg)

# %% [markdown]
# ### An **unrooted phylogenetic tree**, like the following, doesn't include an assumption about the last common ancestor of all sequences:
# * Unrooted trees illustrate the relatedness of the leaf nodes without making assumptions about ancestry.
# * They do not require the ancestral root to be known or inferred.
# * Unrooted trees can always be generated from rooted ones by simply omitting the root.
# * By contrast, inferring the root of an unrooted tree requires some means of identifying ancestry.
# * This is normally done by including an outgroup in the input data so that the root is necessarily between the outgroup and the rest of the taxa in the tree, or by introducing additional assumptions about the relative rates of evolution on each branch, such as an application of the molecular clock hypothesis.
#
# ![](./images/basic-unrooted-tree1.jpg)

# %% [markdown]
# A fun example:
#
# ![image14.png](./images/image14.png)
#
# Unrooted phylogenetic tree of hypervariable segment 1 (HVS-I) variation in mitochondrial DNA haplogroup R0 and R0b in humans.
#
# What's the deal with mitochondria?

# %% [markdown]
# ### What about horizontal gene transfer?
#
# * Horizontal gene transfer (HGT) or lateral gene transfer (LGT) is the movement of genetic material between unicellular and/or multicellular organisms other than by the ("vertical") transmission of DNA from parent to offspring.
# * Horizontal gene transfer is a primary mechanism for the spread of antibiotic resistance in bacteria, plays an important role in the evolution of bacteria that can degrade novel compounds such as human-created pesticides and in the evolution, maintenance, and transmission of virulence.
# * Most thinking in genetics has focused upon vertical transfer, but horizontal gene transfer is important, and among single-celled organisms is perhaps the dominant form of genetic transfer.
#
# ![image10.png](./images/image10.png)
#
# ![image11.png](./images/image11.png)
#
# Tree of life showing vertical and horizontal gene transfers:
#
# ![image12.png](./images/image12.png)
#
# ![image13.png](./images/image13.png)
#
# A phylogenetic network or reticulation is any graph used to visualize evolutionary relationships (either abstractly or explicitly) between nucleotide sequences, genes, chromosomes, genomes, or species.
# * They are employed when reticulation events such as hybridization, horizontal gene transfer, recombination, or gene duplication and loss are believed to be involved.
# * They differ from phylogenetic trees by the explicit modeling of richly linked networks, by means of the addition of hybrid nodes (nodes with two parents) instead of only tree nodes (a hierarchy of nodes, each with only one parent).
# * Phylogenetic trees are a subset of phylogenetic networks.

# %% [markdown]
# Mini lab 01
# ===========
#
# Improving simulation assumptions
#
# Q1
# --
# * Does our ability to re-construct improve with sequence length?
#   Show a couple tests to see, using hamming/kmer and/or upgma/nj.
#   Note: you don't need a totally computational metric of quality, but at least couple plots where you manually assess quality for different starting parent sequence lengths.
#     * Does it matter the method?
#     * If it changes, is the function linear, increasing, decreasing?

# %%
# Do coding here

# %% [markdown]
# Q2
# --
# * Does our ability to re-construct improve with mutation rates that are uneven across the DNA segment, e.g., some regions being hyper-variable, and others being less so?

# %%
# Do coding here

# %% [markdown]
# ## References
# The material in this section was compiled while consulting the following sources:
#
# 1. The Phylogenetic Handbook (Lemey, Salemi, Vandamme)
# 2. Inferring Phylogeny (Felsenstein)
