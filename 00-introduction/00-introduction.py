# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     cell_metadata_filter: title,-all
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# ![opening](./images/bioinfo.jpg)
#
# # Introduction
#
# Bioinformatics is the application of the tools of computer science (such as programming languages, algorithms, and databases) to address biological problems (for example, inferring the evolutionary relationship between a group of organisms based on fragments of their genomes, or understanding if or how the community of microorganisms that live in my gut changes if I modify my diet). Bioinformatics is a rapidly growing field, largely in response to the vast increase in the quantity of data that biologists now grapple with. Students from varied disciplines (e.g., biology, computer science, statistics, and biochemistry) and stages of their educational careers (undergraduate, graduate, or postdoctoral) are becoming interested in bioinformatics.
#
# In this book, we make extensive use of common Python libraries, such as [scikit-learn](https://scikit-learn.org/) and [scikit-bio](http://www.scikit-bio.org), which provide production-ready implementations of algorithms and data structures taught in the text. Readers therefore learn the concepts in the context of tools they can use to develop their own bioinformatics software and pipelines, enabling them to rapidly get started on their own projects. While some theory is discussed, the focus is on what readers need to know to be effective, practicing bioinformaticians.
#
# One goal is to make getting started in bioinformatics as accessible as possible to students from varied backgrounds, and to get more people interested in this exciting field.
#
# ## Who should read this?
#
# These scripts (documents) written for students interested in understanding and applying bioinformatics methods, and ultimately in developing their own bioinformatics analysis pipelines or software.
#
# We assume little background in biology or computer science, however some basic background is very helpful. For example, an understanding of the roles of and relationship between DNA and protein in a cell, and the ability to read and follow well-annotated Python3 code, are both helpful (but not necessary) to get started.
#
# ## How to read this
#
# 0. Install jupyter notebook and jupytext
# 1. Open each python file (jupytext format) in jupyter notebook.
# 2. As you read, code blocks will be presented throughout the book, and you'll have to run the code cell to see the output.
#
# Here's an example Python 3 code cell that imports the scikit-bio library and prints some information to the screen.

# %%
import skbio  # type: ignore

print(skbio.title)
print(skbio.art)

# %% [markdown [markdown]
# To make your jupyter notebooks full-width display in the browser, I suggest: `~/.jupyter/custom/custom.css` containing:
#
# ```css
# /* Make the notebook cells take almost all available width */
# .container {
#     width: 99% !important;
# }
#
# /* Prevent the edit cell highlight box from getting clipped;
#  * important so that it also works when cell is in edit mode*/
# div.cell.selected {
#     border-left-width: 1px !important;
# }
# ```

# %% [markdown]
# We'll inspect a lot of source code as we explore bioinformatics algorithms. If you're ever interested in seeing the source code for some functionality that we're using, you can do that using IPython's ``psource`` magic.

# %%
# %psource skbio.TabularMSA.conservation

# %%
# Or directly
import inspect

print(inspect.getsource(skbio.TabularMSA.conservation))

# %% [markdown]
# The documentation for scikit-bio is also very extensive (though the package itself is still in early development). You can view the documentation for the `TabularMSA` object, for example, [here](http://scikit-bio.org/docs/latest/generated/skbio.alignment.TabularMSA.html). These documents will be invaluable for learning how to use the objects.
