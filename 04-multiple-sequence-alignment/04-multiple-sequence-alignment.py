# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.11.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Generalized dynamic programming for multiple sequence alignment (MSA)

# %% [markdown]
# Do we want to use global or local methods for MSA?
# ==================================================
#
# ![image0.png](./images/image0.png)

# %% [markdown]
# Which types of genes will diverge less over evolutionary time?
# ==============================================================
#
# ![image1.png](./images/image1.png)

# %% [markdown]
# A gene family is a set of several similar genes, formed by duplication
# of a single original gene, and generally with similar biochemical
# functions.
#
# ![image2.png](./images/image2.png)
#
# Where does our 16s ribosome fit into this spectrum of likely-to-mutate or be conserved over evolutionary time?
# ==============================================================================================================
#
# Until now we worked with alignments between two sequences, but it is likely that you will want to align many sequences at the same time. For example, if you are trying to gain insight on the evolutionary relationships between all of the 16S bacterial genes in a given sample, it would be time consuming and very inefficient to compare them two at a time. It would be more efficient and useful to compare all of the 16S sequences from the bacteria in the same alignment.
#
# In the pairwise sequence alignment chapter, we went over dynamic programming algorithms. It's possible to generalize Smith-Waterman and Needleman-Wunsch, the dynamic programming algorithms that we explored for pairwise sequence alignment, to identify the optimal alignment of more than two sequences. Remember that our scoring scheme for pairwise alignment looked like the following:

# %% [markdown]
# $$
# \begin{align}
# & F(0, 0) = 0\\
# & F(i, 0) = F(i-1, 0) - d\\
# & F(0, j) = F(0, j-1) - d\\
# \\
# & F(i, j) = max \begin{pmatrix}
# & F(i-1, j-1) + s(c_i, c_j)\\
# & F(i-1, j) - d\\
# & F(i, j-1) - d)\\
# \end{pmatrix}
# \end{align}
# $$

# %% [markdown]
# To generalize this to three sequences, we could create $3 \times 3$ scoring, dynamic programming, and traceback matrices. Our scoring scheme would then look like the following:

# %% [markdown]
# $$
# \begin{align}
# & F(0, 0, 0) = 0\\
# & F(i, 0, 0) = F(i-1, 0, 0) - d\\
# & F(0, j, 0) = F(0, j-1, 0) - d\\
# & F(0, 0, k) = F(0, 0, k-1) - d\\
# \\
# & F(i, j, k) = max \begin{pmatrix}
# F(i-1, j-1, k-1) + s(c_i, c_j) + s(c_i, c_k) + s(c_j, c_k)\\
# F(i, j-1, k-1) + s(c_j, c_k) - d\\
# F(i-1, j, k-1) + s(c_i, c_k) - d\\
# F(i-1, j-1, k) + s(c_i, c_j) - d\\
# F(i, j, k-1) - 2d\\
# F(i, j-1, k) - 2d\\
# F(i-1, j, k) - 2d\\
# \end{pmatrix}
# \end{align}
# $$

# %% [markdown]
# However the complexity of this algorithm is much worse than for pairwise alignment. For pairwise alignment, remember that if aligning two sequences of lengths $m$ and $n$, the runtime of the algorithm will be proportional to $m \times n$. If $n$ is longer than or as long as $m$, we simplify the statement to say that the runtime of the algorithm will be be proportional to $n^2$. This curve has a pretty scary trajectory: runtime for pairwise alignment with dynamic programming is said to scale quadratically.

# %%
import matplotlib.pyplot as plt
import functools
import skbio
import scipy as sp
from scipy import cluster
from typing import Callable

from IPython.core import page

page.page = print

# %%

seq_lengths = range(25)
s2_times = [t**2 for t in range(25)]
print(s2_times)
plt.plot(range(25), s2_times)
plt.xlabel("Sequence Length")
plt.ylabel("Runtime (s)")

# %% [markdown]
# The exponent in the $n^2$ term comes from the fact that, in pairwise alignment, if we assume our sequences are both of length $n$, there are $n \times n$ cells to fill in in the dynamic programming matrix. If we were to generalize either Smith-Waterman or Needleman-Wunsch to three sequences, we would need to create a 3 dimensional array to score and trace back the alignment. For sequences of length $n$, we would therefore have $n \times n \times n$ cells to fill in, and our runtime versus sequence length curve would look like the following.

# %%
s3_times = [t**3 for t in range(25)]

plt.plot(range(25), s3_times)
plt.xlabel("Sequence Length")
plt.ylabel("Runtime (s)")

# %% [markdown]
# That curve looks steeper than the curve for pairwise alignment, and the values on the y-axis are bigger, but it's not really clear how much of a problem this is until we plot runtime for three sequences in the context of the run times for pairwise alignment.

# %%
plt.plot(range(25), s2_times)
plt.plot(range(25), s3_times)
plt.xlabel("Sequence Length")
plt.ylabel("Runtime (s)")

# %% [markdown]
# And for four sequences:

# %%
s4_times = [t**4 for t in range(25)]

plt.plot(range(25), s2_times)
plt.plot(range(25), s3_times)
plt.plot(range(25), s4_times)
plt.xlabel("Sequence Length")
plt.ylabel("Runtime (s)")

# %% [markdown]
# We clearly have a problem here, and that is that the runtime for multiple sequence alignment using full dynamic programming algorithms grows exponentially with the number of sequences to be aligned. If $n$ is our sequence length, and $s$ is the number of sequences, that means that runtime is proportional to $n^s$. In pairwise alignment, $s$ is always equal to 2, so the problem is more manageable. However, for the general case of $s$ sequences, we really can't even consider Smith-Waterman or Needleman-Wunsch for more than just a few sequences. The pattern in the plots above should illustrate why.
#
# ![image3.png](./images/image3.png)
#
# As we explored with database searching, we need to figure out how to align fewer sequences. This is where *progressive alignment* comes in.

# %% [markdown]
# #### ======== Cahoot-09-1
# https://umsystem.instructure.com/courses/97521/quizzes/288426

# %% [markdown]
# ## Progressive alignment
#
# In progressive alignment, the problem of exponential growth of runtime and space is managed by selectively aligning pairs of sequences, and aligning alignments of sequences.
#
# * What we typically do is identify a pair of closely related sequences, and align those.
# * Then, we identify the next most closely related sequence to that initial pair, and align that sequence to the alignment.
# * This concept of aligning a sequence to an alignment is new, and we'll come back to it in just a few minutes.
# * The other concept of identifying the most closely related sequences, and then the next most closely related sequence, and so on should sound familiar.
# * It effectively means that we're traversing a tree.
# * And herein lies our problem: we need a tree to efficiently align multiple sequences, but we need an alignment to build a good tree.
#
# As a quick review of tree terminology:
# ![image4.png](./images/image4.png)
#
# ![image5.png](./images/image5.png)
#
# You probably have two burning questions in your mind right now:

# %% [markdown]
# 1. How do we build a tree to guide the alignment process, if we need an alignment to build a good tree?
# 2. How do we align a sequence to an alignment, or an alignment to an alignment?

# %% [markdown]
# We'll explore both of those through-out the rest of this notebook.
#
# First, let's cover the process of progressive multiple sequence alignment, just assuming for a moment that we know how to do both of those things.

# %% [markdown]
# The process of progressive multiple sequence alignment could look like the following.
# * First, we start with some sequences and a tree representing the relationship between those sequences.
#     * We'll call this our guide tree, because it's going to guide us through the process of multiple sequence alignment.
# * In progressive multiple sequence alignment, we build a multiple sequence alignment for each internal node of the tree, where the alignment at a given internal node contains all of the sequences in the clade defined by that node.

# %% [markdown]
# ![](./images/msa-tree-input.png)

# %% [markdown]
# * Starting from the root node, descend the bottom branch of the tree until you get to the an internal node.
# * If an alignment hasn't been constructed for that node yet, continue descending the tree until getting to a pair of nodes.
# * In this case, we follow the two branches to the tips.
# * We then align the sequences at that pair of tips (usually with Needleman-Wunsch, for multiple sequence alignment), and assign that alignment to the node connecting those tips (`s4` and `s5` below).

# %% [markdown]
# ![](./images/msa-tree-a1.png)

# %% [markdown]
# * Next, we want to find what to align the resulting alignment to, so start from the root node and descend the top branch of the tree.
# * When you get to the next node, determine if an alignment has already been created for that node.
# * If not, our job is to build that alignment so we have something to align against.
# * In this case, that means that we need to align `s1`, `s2`, and `s3`.
# * We can achieve this by aligning `s1` and `s3` first, to get the alignment at the internal node connecting them.

# %% [markdown]
# ![](./images/msa-tree-a2.png)

# %% [markdown]
# We can next align the alignment of `s1` and `s3` with `s2`, to get the alignment at the internal node connecting those clades.

# %% [markdown]
# ![](./images/msa-tree-a3.png)

# %% [markdown]
# And finally, we can compute the alignment at the root node of the tree, by aligning the alignment of `s1`, `s2`, and `s3` with the alignment of `s4` and `s5`.

# %% [markdown]
# ![](./images/msa-tree-final.png)

# %% [markdown]
# ## Recall data structures: What kind of tree traversal produces such a pattern of emumeration?

# %% [markdown]
# The alignment at the root node is our multiple sequence alignment.
#
# ### Building the guide tree
#
# #### Side-note: The Iris flower data set or Fisher's Iris data set
#
# -   a multivariate data set introduced by the British statistician and
#     biologist Ronald Fisher in his 1936 paper, The use of multiple
#     measurements in taxonomic problems as an example of linear
#     discriminant analysis.
# -   It is sometimes called Anderson's Iris data set because Edgar
#     Anderson collected the data to quantify the morphologic variation of
#     Iris flowers of three related species.
# -   Two of the three species were collected in the Gaspé Peninsula "all
#     from the same pasture, and picked on the same day and measured at
#     the same time by the same person with the same apparatus".
#
# The data set consists of 50 samples from each of three species of Iris
# (Iris setosa, Iris virginica and Iris versicolor).
#
# -   Four features were measured from each sample: the **length** and the
#     **width** of the **sepals** and **petals**, in centimetres.
# -   Based on the combination of these four features, Fisher developed a
#     linear discriminant model to distinguish the species from each
#     other.
#
# ![image6.png](./images/image6.png)

# %% [markdown]
# Hierarchical clustering:
# <a href="http://scikit-learn.org/stable/modules/clustering.html#hierarchical-clustering" class="uri">http://scikit-learn.org/stable/modules/clustering.html#hierarchical-clustering</a>
# ![image7.png](./images/image7.png)
#
# Let's address the first of our outstanding questions. I mentioned above that *we need an alignment to build a good tree*. The key word here is *good*. We can build a very rough tree - one that we would never want to present as representing the actual relationships between the sequences in question - without first aligning the sequences. Building a UPGMA tree requires only a distance matrix, so if we can find a non-alignment-dependent way to compute distances between the sequences, we can build a rough UPGMA tree from them.
#
# <a href="https://en.wikipedia.org/wiki/UPGMA" class="uri">https://en.wikipedia.org/wiki/UPGMA</a>
#
# <a href="https://en.wikipedia.org/wiki/Hierarchical_clustering" class="uri">https://en.wikipedia.org/wiki/Hierarchical_clustering</a>
#
# Assuming points plotted on x, y axes, here are some 2D raw data (Differing in x y space):
#
# ![image8.png](./images/image8.png)

# %% [markdown]
# Based on distance (of a kind) in the above space, make a top-down (bottom up) cluster of the raw data:
#
# ![image9.png](./images/image9.png)
#
# * Let's compute distances between the sequences based on their *word* composition (recall: k-tuple / k-word / k-mer).
# * We'll define a *word* here as `k` adjacent characters in the sequence.
# * We can then define a function that will return all of the words in a sequence as follows.
#     * These words can be defined as being overlapping, or non-overlapping.
#     * We'll go with overlapping for this example, as the more words we have, the better our guide tree should be.

# %% [markdown]
# #### ======== Cahoot-09-2
# https://umsystem.instructure.com/courses/97521/quizzes/288427

# %%
# %psource skbio.DNA.iter_kmers

# %% [markdown]
# What is the cost of too-short k-mers?
# =====================================

# %%
for e in skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGTA").iter_kmers(k=3):
    print(e)

# %%
# Fewer without overlaps
for e in skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGTA").iter_kmers(k=3, overlap=False):
    print(e)

# %% [markdown]
# What is the cost of too long k-mers?
# ====================================

# %%
for e in skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGTA").iter_kmers(k=7):
    print(e)

# %% [markdown]
# If we then have two sequences, we can compute the word counts for each
# and define a distance between the sequences as:
#
# * **the fraction of words that are unique to either sequence.**
# With a function defined below.
#
#
# What other definitions could we have?
# =====================================
#
# Can we incorporate count too?
# =============================

# %%
# For a review of apply: help(map)
print(type(skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGTA").iter_kmers(k=5, overlap=True)))

set1 = set(
    map(str, skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGTA").iter_kmers(k=5, overlap=True))
)
set2 = set(
    map(str, skbio.DNA(sequence="AACGTGACCAGTTCGACCATTA").iter_kmers(k=5, overlap=True))
)

print("\nAll: \n", set1 | set2, "\n")
print("Shared: \n", set1 & set2)

# %%
def kmer_distance(sequence1, sequence2, k=3, overlap=True):
    """Compute the kmer distance between a pair of sequences

    Parameters
    ----------
    sequence1 : skbio.Sequence
    sequence2 : skbio.Sequence
    k : int, optional
        The word length.
    overlapping : bool, optional
        Defines whether the k-words should be overlapping or not
        overlapping.

    Returns
    -------
    float
        Fraction of the set of k-mers from both sequence1 and
        sequence2 that are unique to either sequence1 or
        sequence2.

    Raises
    ------
    ValueError
        If k < 1.

    Notes
    -----
    k-mer counts are not incorporated in this distance metric.

    """
    sequence1_kmers = set(map(str, sequence1.iter_kmers(k=k, overlap=overlap)))
    sequence2_kmers = set(map(str, sequence2.iter_kmers(k=k, overlap=overlap)))
    all_kmers = sequence1_kmers | sequence2_kmers
    shared_kmers = sequence1_kmers & sequence2_kmers
    number_unique = len(all_kmers) - len(shared_kmers)
    fraction_unique = number_unique / len(all_kmers)
    return fraction_unique


# %% [markdown]
# ### We can then use this k-mer metric as an abstract distance function.
#
# ### In clustering, distance can be in an abstract space, or n-dimensional:
#
# -   <a href="https://en.wikipedia.org/wiki/Metric_(mathematics)" class="uri">https://en.wikipedia.org/wiki/Metric_(mathematics)</a>
# -   <a href="https://en.wikipedia.org/wiki/Distance" class="uri">https://en.wikipedia.org/wiki/Distance</a>
#
# ![image10.png](./images/image10.png)
#
# What would happen if we included the 4th, Sepal Width?
# ======================================================
#
# To see how to implement bottom up agglomerative hierarchical clustering,
# check out this small-medium implementation in Python:
#
# <a href="https://github.com/ZwEin27/Hierarchical-Clustering" class="uri">https://github.com/ZwEin27/Hierarchical-Clustering</a>

# %%
help(skbio.DNA.distance)

# %%
# %psource skbio.DNA.distance

# %%
s1 = skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGT")
s2 = skbio.DNA(sequence="ATCGGTACCGGTAGAAGT")
s3 = skbio.DNA(sequence="GGTACCAAATAGAA")

# DNA.distance is just applying here:
# 3 optional default
print(kmer_distance(sequence1=s1, sequence2=s2))
# or:
print(s1.distance(s2, metric=kmer_distance))

print(kmer_distance(sequence1=s1, sequence2=s3))
# or
print(s1.distance(s3, metric=kmer_distance))

# If we wanted to include a k
print(kmer_distance(sequence1=s1, sequence2=s3, k=3))
# Alternative to this below:

# %% [markdown]
# Another way: if we wanted to override the default to create (for example) a 5-mer distance function, we could use ``functools.partial``.

# %%
fivemer_distance = functools.partial(kmer_distance, k=5)

s1 = skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGT")
s2 = skbio.DNA(sequence="ATCGGTACCGGTAGAAGT")
s3 = skbio.DNA(sequence="GGTACCAAATAGAA")

print(s1.distance(s2, metric=fivemer_distance))
print(s1.distance(s3, metric=fivemer_distance))

# %% [markdown]
# We can now apply one of these functions to build a distance matrix for a
# set of sequences that we want to align.
#
# ![image11.png](./images/image11.png)

# %% [markdown]
# Suppose these data are to be analyzed, where pixel Euclidean distance is
# the distance metric.
#
# |  \- |    a   |     b     |     c    |     d     |     e    |   f   |
# |:---:|:------:|:---------:|:--------:|:---------:|:--------:|:-----:|
# |  a  |  0 \|  | 184 \|    |  222 \|  |   177 \|  |  216 \|  | 231\| |
# |  b  | 184 \| |    0 \|   | 45 \|    |  123 \|   |  128 \|  | 200\| |
# |  c  | 222 \| |   45 \|   |  0  \|    | 129 \|    |  121 \|  | 203\| |
# |  d  | 177 \| |   123 \|  |  129 \|  |    0 \|   | 46 \|    |  83\| |
# |  e  | 216 \| |   128 \|  |  121 \|  |   46 \|   |  0 \|    |  83\| |
# |  f  | 231 \| |   200 \|  |  203 \|  |   83 \|   |  83 \|   |  0\|  |
#
# These data can then be viewed in graphic form as a heat map. In this
# image, black denotes a distance of 0 and white is maximal distance:
#
# ![image12.png](./images/image12.png)
#
# Do we need both the top half and bottom half, above and below the diagonal?

# %% [markdown]
# <a href="http://www.slimsuite.unsw.edu.au/teaching/upgma/" class="uri">http://www.slimsuite.unsw.edu.au/teaching/upgma/</a>
#
# * The images below include a walkthrough of clustering 7 biological sequences (A-G) using the Unweighted Pair-Group Method with Arithmetic mean (UPGMA) method.
# * UPGMA is actually a generic method and thus the walkthrough could apply to any objects A-G for which pairwise distances can be calculated.
# * UPGMA is a distance method and therefore needs a distance matrix.
# * UPGMA is "ultrametric", meaning that all the terminal nodes (i.e. the sequences/taxa) are equally distant from the root.
# * In molecular terms, this means that UPGMA assumes an even molecular clock, i.e. all lineages are evolving at a constant rate.
# * In practical terms, this means that you can construct a distance scale bar and all the terminal nodes will be level at position 0.0, representing the present.
# * In this example, the scale bar is shown on the right-hand side.
#
# ![image13.png](./images/image13.png)

# %% [markdown]
# ![image14.png](./images/image14.png)
#
# Each round of UPGMA follows the same pattern.
#
# 1.  Identify the shortest pairwise distance in the matrix. This
#     identifies the two sequences to be clustered.
#
# 2.  Join the two sequences identified.
#
# 3.  The pair should be linked at a depth that is half of the shortest
#     pairwise distance.
#
# 4.  The tip-to-tip distance between the joined elements will equal the
#     shortest distance.

# %% [markdown]
# ![image15.png](./images/image15.png)
#
# -   The two sequences joined (B and F) are removed from the original
#     matrix and replaced by the new clade (BF).
#
# -   Each distance between BF and the other sequences (A, C, D, E and G)
#     is the mean distance between them and B and F from the original
#     matrix.
#
# E.g., $d(A, BF) = (d(A, B) + d(A, F))/2$

# %% [markdown]
# ![image16.png](./images/image16.png)
#
# -   Identify the shortest pairwise distance in the new matrix.
#
# -   As before, join the two items at a depth equal to half the pairwise
#     distance.

# %% [markdown]
# ![image17.png](./images/image17.png)
#
# -   Each time the new matrix is made, distance are taken from the
#     original matrix (top).
#
# -   All of the values to be replaced in the new matrix will come from
#     the rows and columns corresponding to the sequences in the new clade
#     (A and D), highlighted by the dashed orange boxes.

# %% [markdown]
# ![image18.png](./images/image18.png)
#
# -   As before, mean values are calculated using the individual pairwise
#     values from the original matrix.
#
# -   Where the new cluster (AD) is being compared to the previous cluster
#     (BF), all pairwise combinations between the groups are used for the
#     calculation,
#
# E.g., $d(AD, BF) = (d(A, B) + d(A, F) + d(D, B) + d(D, F)) / 4$

# %% [markdown]
# ![image19.png](./images/image19.png)
#
# -   Again, the shortest pairwise distance in the new matrix is used to
#     identify the groups/sequences to be clustered.
#
# -   In this case, sequence G is added to the BF clade.
#
# -   Again, the depth of the join is half the pairwise distance.
#
# -   The tip-to-tip paths from B or F to G equals the full distance.

# %% [markdown]
# ![image20.png](./images/image20.png)
#
# -   Again, when generating the new matrix, use the pairwise distances
#     from the original matrix.
# -   UPGMA is unweighted, so all pairwise distances contribute equally.
# -   This means that the distance between BFG and AD is the mean of all
#     six possible pairwise combinations.

# %% [markdown]
# ![image21.png](./images/image21.png)
#
# -   The rest of the new BFG columns and rows are calculated as the mean
#     distances of B, F and G with the remaining sequences C and E.
# -   Note that all the pairwise distances in the rows and columns for B,
#     F and G are either used for calculating the new means (coloured
#     boxes) or are internal distances within the BFG clade (red and green
#     text).

# %% [markdown]
# ![image22.png](./images/image22.png)
#
# -   The cycle is repeated as before.
# -   This time, no new sequences are added but the two existing clades
#     (AD and BFG) are joined at a depth of half the mean pairwise
#     distance between AD and BFG.
# -   All possible tip-to-tip distances between A/D and B/F/G add up to
#     the full distance.

# %% [markdown]
# ![image23.png](./images/image23.png)
#
# Again, the distance matrix shrinks by one and mean distances are
# calculated for the new clade, ADBFG.

# %% [markdown]
# ![image24.png](./images/image24.png)
#
# Make penultimate join using shortest pairwise distance in new matrix, as
# previously.

# %% [markdown]
# ![image25.png](./images/image25.png)
#
# -   Once the final join has been made, the UPGMA tree is complete.
# -   UPGMA is inherently rooted and thus the root is placed at the
#     deepest point of the tree, at a depth of half the final mean
#     pairwise distance.
# -   All root to tip distances are the same, meaning that this method
#     assumes a molecular clock for sequence data, i.e., a constant rate
#     of evolution throughout the tree.
# -   In more general terms (e.g. for non-molecular data), such a tree is
#     referred to as "ultrametric".

# %% [markdown]
# ![image26.png](./images/image26.png)
#
# -   Once the UPGMA method is finished, all the pairwise distances in the
#     original matrix will have contributed to one and only one of the
#     shortest distances used in the clustering.
# -   These are colour coded in the example.
# -   E.g., the two green pairwise distances, $d(B,G)$ and $d(F,G)$,
#     generated the distance 12.50 used in the third cycle to join BF
#     and G.

# %% [markdown]
# ![image27.png](./images/image27.png)
#
# For clarity, this data represents only a subset of the taxa included in
# the original Fitch & Margoliash paper:
# <a href="http://www.ncbi.nlm.nih.gov/pubmed/5334057" class="uri">http://www.ncbi.nlm.nih.gov/pubmed/5334057</a>

# %% [markdown]
# ![image28.png](./images/image28.png)
#
# -   In this example, therefore, human (B) and monkey (F) are the closest
#     pair, which next group with dog (G) (the other mammal), then the
#     chicken (D)/turtle (A) (the other Amniota), then tuna (fish) (C) to
#     form a vertebrate clade and finally moth (insect) (E).
# -   Based on this data, Cytochrome C supports the known phylogenetic
#     relationship of these organisms.
# -   In the original paper, they get the same relationship for these
#     organisms (and more!) using a different method.

# %% [markdown]
# #### +++++++++++++++ Cahoot-09-3
# https://umsystem.instructure.com/courses/97521/quizzes/288428

# %% [markdown]
# ### The cytochrome complex, or cyt c is a small heme-protein found loosely associated with the inner membrane of the mitochondrion.
#
# * Cytochrome c is a component of the electron transport chain in mitochondria.
# * Cytochrome c also has an intermediate role in apoptosis, a controlled form of programmed cell death used to selectively kill cells in the process of development, or in response to infection or DNA damage.
#
# Side note: When graphing the protein interaction network in relation to
# all cancers, the mitochondrial proteins are central.
#
# ![image29.png](./images/image29.png)
#
# The electron transport chain in the mitochondrion is the site of
# oxidative phosphorylation in eukaryotes. The NADH and succinate
# generated in the citric acid cycle are oxidized, providing energy to
# power ATP synthase.

# %% [markdown]
# UPGMA summary ![image30.png](./images/image30.png)
#
# We can next use some functionality from SciPy to cluster bio sequences
# with UPGMA, and print out a dendrogram.
# %%
query_sequences = [
    skbio.DNA(sequence="ACCGGTGACCAGTTGACCAGT", metadata={"id": "s1"}),
    skbio.DNA(sequence="ATCGGTACCGGTAGAAGT", metadata={"id": "s2"}),
    skbio.DNA(sequence="GGTACCAAATAGAA", metadata={"id": "s3"}),
    skbio.DNA(sequence="GGCACCAAACAGAA", metadata={"id": "s4"}),
    skbio.DNA(sequence="GGCCCACTGAT", metadata={"id": "s5"}),
]

# %%
help(skbio.DistanceMatrix.from_iterable)

# %%
guide_dm = skbio.DistanceMatrix.from_iterable(
    iterable=query_sequences, metric=kmer_distance, key="id"
)

print(guide_dm)

# %% [markdown]
# scikit-bio also has some basic visualization functionality for these objects. For example, we can easily visualize this object as a heatmap.

# %%
fig = guide_dm.plot(cmap="Greens")

# %%
help(sp.cluster.hierarchy.average)

# %%
help(sp.cluster.hierarchy.linkage)

# %%
help(sp.cluster.hierarchy.dendrogram)

# %%
help(sp.cluster.hierarchy.to_tree)

# %%
# guide_dm is the distance matrix above, which we can plot as a color heatmap
fig = guide_dm.plot(cmap="Greens")

# %%
print("Matrix form: \n", guide_dm)

# Toss the matrix above the diagonal. Assumes symmetry
print("\n Condensed: \n", guide_dm.condensed_form())
print(
    "\n Clustering on the condensed \n",
    sp.cluster.hierarchy.average(y=guide_dm.condensed_form()),
)

# %% [markdown]
# For Average() data format:
# * The distance between clusters ``Z[i, 0]`` and ``Z[i, 1]`` is given by ``Z[i, 2]``.
# * The fourth value ``Z[i, 3]`` represents the number of original observations in the newly formed cluster.

# %%
help(skbio.DistanceMatrix)

# %%
help(skbio.DistanceMatrix.condensed_form)

# %%
for q in query_sequences:
    print(q)

guide_lm = sp.cluster.hierarchy.average(y=guide_dm.condensed_form())
print("\nguide lm\n", guide_lm)

guide_d = sp.cluster.hierarchy.dendrogram(
    Z=guide_lm,
    labels=guide_dm.ids,
    orientation="right",
    link_color_func=lambda x: "black",
)
print("\nguide_d\n", guide_d)

guide_tree = sp.cluster.hierarchy.to_tree(Z=guide_lm)


# %%
def guide_tree_from_sequences(
    sequences: list[skbio.Sequence],
    metric: Callable[
        [skbio.Sequence, skbio.Sequence, int, bool], float
    ] = kmer_distance,
    display_tree: bool = False,
) -> sp.cluster.hierarchy.ClusterNode:
    """Build a UPGMA tree by applying metric to sequences

    Parameters
    ----------
    sequences : list of skbio.Sequence objects (or subclasses)
      The sequences to be represented in the resulting guide tree.
    metric : function
      Function that returns a single distance value when given a pair of
      skbio.Sequence objects.
    display_tree : bool, optional
      Print the tree before returning.

    Returns
    -------
    skbio.TreeNode

    """
    guide_dm = skbio.DistanceMatrix.from_iterable(
        iterable=sequences, metric=metric, key="id"
    )
    guide_lm = sp.cluster.hierarchy.average(y=guide_dm.condensed_form())
    guide_tree = sp.cluster.hierarchy.to_tree(Z=guide_lm)
    if display_tree:
        guide_d = sp.cluster.hierarchy.dendrogram(
            Z=guide_lm,
            labels=guide_dm.ids,
            orientation="right",
            link_color_func=lambda x: "black",
        )
    return guide_tree


# %%
t = guide_tree_from_sequences(sequences=query_sequences, display_tree=True)

# %% [markdown]
# We now have a guide tree, so we can move on to the next step of progressive alignment.

# %% [markdown]
# ### Generalization of Needleman-Wunsch (with affine gap scoring) for progressive multiple sequence alignment

# %% [markdown]
# * Next, we'll address our second burning question: aligning alignments.
# * As illustrated above, there are basically three different types of pairwise alignment we need to support for progressive multiple sequence alignment with Needleman-Wunsch:
#     1. Alignment of a pair of sequences.
#     2. Alignment of a sequence and an alignment.
#     3. Alignment of a pair of alignments.

# %% [markdown]
# * Standard Needleman-Wunsch supports the first, and it is very easy to generalize it to support the latter two.
# * The only change that is necessary is in how the alignment of two non-gap characters is scored.
# * Recall that we previously scored an alignment of two characters by looking up the score of substitution from one to the other in a substitution matrix.
# * To adapt this for aligning a sequence to an alignment, or for aligning an alignment to an alignment, we compute this substitution as the average score of aligning the pairs of characters.

# %% [markdown]
# For example, if we want to align the alignment column from $aln1$:

# %% [markdown]
# ```
# A
# C
# ```

# %% [markdown]
# to the alignment column from $aln2$:

# %% [markdown]
# ```
# T
# G
# ```

# %% [markdown]
# Where `n_seq` is the number of sequences for a given alignment, we could compute the substitution score using the matrix $m$ as:

# %% [markdown]
# $$
# s = \frac{m[A][T] + m[A][G] + m[C][T] + m[C][G]}{aln1_{nseq} \times aln2_{nseq}}
# $$
#
# Why multiplication?
# ===================
#
# How many $m[][]$ lookups for an alignment between two alignments, each
# with 3 pre-aligned sequences within them?

# %%
print(skbio.alignment._pairwise._traceback_encoding)

# %%
# %psource skbio.alignment._pairwise._compute_score_and_traceback_matrices

# %%
# %psource skbio.alignment._pairwise._compute_substitution_score

# %%
# %psource skbio.alignment._pairwise._traceback

# %%
# %psource skbio.alignment.global_pairwise_align_nucleotide

# %%
# %psource skbio.alignment.global_pairwise_align

# %% [markdown]
# * The following code adapts our implementation of Needleman-Wunsch to support aligning a sequence to an alignment, or aligning an alignment to an alignment.
# * For the sake of the examples below, we're overriding one of the ``global_pairwise_align_nucleotide`` defaults to penalize terminal gaps.
#     * This effectively tells the algorithm that we know we have a collection of sequences that are homologous from beginning to end.

# %%
global_pairwise_align_nucleotide = functools.partial(
    skbio.alignment.global_pairwise_align_nucleotide, penalize_terminal_gaps=True
)

# %% [markdown]
# For example, we can still use this code to align pairs of sequences (but note that we now need to pass those sequences in as a pair of one-item lists):

# %%
aln1, _, _ = global_pairwise_align_nucleotide(
    seq1=query_sequences[0], seq2=query_sequences[1]
)
print(aln1)

# %% [markdown]
# We can align that alignment to one of our other sequences.

# %%
aln1, _, _ = global_pairwise_align_nucleotide(seq1=aln1, seq2=query_sequences[2])
print(aln1)

# %% [markdown]
# Alternatively, we can align another pair of sequences:

# %%
aln2, _, _ = global_pairwise_align_nucleotide(
    seq1=query_sequences[2], seq2=query_sequences[3]
)
print(aln2)

# %% [markdown]
# And then align that alignment against our previous alignment:

# %%
aln3, _, _ = global_pairwise_align_nucleotide(seq1=aln1, seq2=aln2)
print(aln3)

# %% [markdown]
# #### +++++++++++++++ Cahoot-09-4
# https://umsystem.instructure.com/courses/97521/quizzes/288429

# %% [markdown]
# ### Putting it all together: progressive multiple sequence alignment

# %% [markdown]
# We can now combine all of these steps to:
# * take a set of query sequences,
# * build a guide tree,
# * perform progressive multiple sequence alignment, and
# * return
#     * the guide tree (as a SciPy linkage matrix), and
#     * the alignment.

# %%
# Recall from above:
guide_d = sp.cluster.hierarchy.dendrogram(
    Z=guide_lm,
    labels=guide_dm.ids,
    orientation="right",
    link_color_func=lambda x: "black",
)

# %%
help(skbio.TreeNode.from_linkage_matrix)

# %%
guide_tree = skbio.TreeNode.from_linkage_matrix(
    linkage_matrix=guide_lm, id_list=guide_dm.ids
)

# %% [markdown]
# We can view the guide tree in [Newick format](http://scikit-bio.org/docs/latest/generated/skbio.io.newick.html) as follows:
#
# <a href="https://en.wikipedia.org/wiki/Newick_format" class="uri">https://en.wikipedia.org/wiki/Newick_format</a>

# %%
print(guide_tree)


# %% [markdown]
# Recall - what kind of tree traversal produces this order of operations?
# =======================================================================
#
# Does the alignment happen before or after recursive call ?
# ==========================================================

# %%
# The recursive function:
def progressive_msa(sequences, pairwise_aligner, guide_tree=None, metric=kmer_distance):
    """Perform progressive msa of sequences

    Parameters
    ----------
    sequences : skbio.SequenceCollection
        The sequences to be aligned.
    metric : function, optional
      Function that returns a single distance value when given a pair of
      skbio.Sequence objects. This will be used to build a guide tree if one
      is not provided.
    guide_tree : skbio.TreeNode, optional
        The tree that should be used to guide the alignment process.
    pairwise_aligner : function
        Function that should be used to perform the pairwise alignments,
        for example skbio.alignment.global_pairwise_align_nucleotide. Must
        support skbio.Sequence objects or skbio.TabularMSA objects
        as input.

    Returns
    -------
    skbio.TabularMSA

    """

    if guide_tree is None:
        guide_dm = skbio.DistanceMatrix.from_iterable(
            iterable=sequences, metric=metric, key="id"
        )
        guide_lm = sp.cluster.hierarchy.average(y=guide_dm.condensed_form())
        guide_tree = skbio.TreeNode.from_linkage_matrix(
            linkage_matrix=guide_lm, id_list=guide_dm.ids
        )

    seq_lookup = {s.metadata["id"]: s for i, s in enumerate(sequences)}

    # working our way down, first children may be super-nodes,
    # then eventually, they'll be leaves
    c1, c2 = guide_tree.children

    # Recursive base case
    if c1.is_tip():
        c1_aln = seq_lookup[c1.name]
    else:
        c1_aln = progressive_msa(
            sequences=sequences, pairwise_aligner=pairwise_aligner, guide_tree=c1
        )

    if c2.is_tip():
        c2_aln = seq_lookup[c2.name]
    else:
        c2_aln = progressive_msa(
            sequences=sequences, pairwise_aligner=pairwise_aligner, guide_tree=c2
        )

    # working our way up, doing alignments, from the bottom up
    alignment, _, _ = pairwise_aligner(seq1=c1_aln, seq2=c2_aln)

    # this is a temporary hack as the aligners in skbio 0.4.1 are dropping
    # metadata - this makes sure that the right metadata is associated with
    # the sequence after alignment
    if isinstance(c1_aln, skbio.Sequence):
        alignment[0].metadata = c1_aln.metadata
        len_c1_aln = 1
    else:
        for i in range(len(c1_aln)):
            alignment[i].metadata = c1_aln[i].metadata
        len_c1_aln = len(c1_aln)
    if isinstance(c2_aln, skbio.Sequence):
        alignment[1].metadata = c2_aln.metadata
    else:
        for i in range(len(c2_aln)):
            alignment[len_c1_aln + i].metadata = c2_aln[i].metadata

    # feed alignment back up, for further aligment, or eventually final return
    return alignment


# %%
msa = progressive_msa(
    sequences=query_sequences,
    pairwise_aligner=global_pairwise_align_nucleotide,
    guide_tree=guide_tree,
)
print(msa)

# %% [markdown]
# * We can now build a (hopefully) improved tree from our multiple sequence alignment.
# * First we'll look at our original distance matrix again, and then the distance matrix generated from the progressive multiple sequence alignment.

# %%
# Original, made from kmer distance on sequences, guide tree
fig = guide_dm.plot(cmap="Greens")
# %%
# Current after-alignment tree data
msa_dm = skbio.DistanceMatrix.from_iterable(
    iterable=msa, metric=kmer_distance, key="id"
)
fig = msa_dm.plot(cmap="Greens")

# %% [markdown]
# * The UPGMA trees that result from these alignments are different.
#     * Note both the ID's at left, and the depth of the forks.
# * First we'll look at the guide tree, and then the tree resulting from the progressive multiple sequence alignment.

# %%
d = sp.cluster.hierarchy.dendrogram(
    Z=guide_lm,
    labels=guide_dm.ids,
    orientation="right",
    link_color_func=lambda x: "black",
)

# %%
msa_lm = sp.cluster.hierarchy.average(y=msa_dm.condensed_form())

d = sp.cluster.hierarchy.dendrogram(
    Z=msa_lm, labels=msa_dm.ids, orientation="right", link_color_func=lambda x: "black"
)


# %% [markdown]
# And we can wrap this all up in a single convenience function:

# %%
def progressive_msa_and_tree(
    sequences,
    pairwise_aligner,
    metric=kmer_distance,
    guide_tree=None,
    display_aln=False,
    display_tree=False,
):
    """Perform progressive msa of sequences and build a UPGMA tree
    Parameters
    ----------
    sequences : skbio.SequenceCollection
        The sequences to be aligned.
    pairwise_aligner : function
        Function that should be used to perform the pairwise alignments,
        for example skbio.alignment.global_pairwise_align_nucleotide. Must
        support skbio.Sequence objects or skbio.TabularMSA objects
        as input.
    metric : function, optional
      Function that returns a single distance value when given a pair of
      skbio.Sequence objects. This will be used to build a guide tree if one
      is not provided.
    guide_tree : skbio.TreeNode, optional
        The tree that should be used to guide the alignment process.
    display_aln : bool, optional
        Print the alignment before returning.
    display_tree : bool, optional
        Print the tree before returning.

    Returns
    -------
    skbio.alignment
    skbio.TreeNode

    """
    msa = progressive_msa(
        sequences=sequences, pairwise_aligner=pairwise_aligner, guide_tree=guide_tree
    )

    if display_aln:
        print(msa)

    msa_dm = skbio.DistanceMatrix.from_iterable(iterable=msa, metric=metric, key="id")
    msa_lm = sp.cluster.hierarchy.average(y=msa_dm.condensed_form())
    msa_tree = skbio.TreeNode.from_linkage_matrix(
        linkage_matrix=msa_lm, id_list=msa_dm.ids
    )
    if display_tree:
        print("\nOutput tree:")
        d = sp.cluster.hierarchy.dendrogram(
            msa_lm,
            labels=msa_dm.ids,
            orientation="right",
            link_color_func=lambda x: "black",
        )
    return msa, msa_tree


# %%
msa = progressive_msa(
    sequences=query_sequences,
    pairwise_aligner=global_pairwise_align_nucleotide,
    guide_tree=guide_tree,
)

# %%
msa, tree = progressive_msa_and_tree(
    sequences=query_sequences,
    pairwise_aligner=global_pairwise_align_nucleotide,
    display_tree=True,
    display_aln=True,
)

# %% [markdown]
# ## Progressive alignment versus iterative alignment
#

# %% [markdown]
# * In an iterative alignment, the output tree from the above progressive alignment is used as a guide tree, and the full process repeated.
# * This is performed to reduce errors that result from a low-quality guide tree.

# %%
def iterative_msa_and_tree(
    sequences,
    num_iterations,
    pairwise_aligner,
    metric=kmer_distance,
    display_aln=False,
    display_tree=False,
):
    """Perform progressive msa of sequences and build a UPGMA tree
    Parameters
    ----------
    sequences : skbio.SequenceCollection
       The sequences to be aligned.
    num_iterations : int
       The number of iterations of progressive multiple sequence alignment to
       perform. Must be greater than zero and less than five.
    pairwise_aligner : function
       Function that should be used to perform the pairwise alignments,
       for example skbio.alignment.global_pairwise_align_nucleotide. Must
       support skbio.Sequence objects or skbio.TabularMSA objects
       as input.
    metric : function, optional
      Function that returns a single distance value when given a pair of
      skbio.Sequence objects. This will be used to build a guide tree if one
      is not provided.
    display_aln : bool, optional
       Print the alignment before returning.
    display_tree : bool, optional
       Print the tree before returning.

    Returns
    -------
    skbio.alignment
    skbio.TreeNode

    """
    if num_iterations > 5:
        raise ValueError(
            "A maximum of five iterations is allowed."
            "You requested %d." % num_iterations
        )
    previous_iter_tree = None
    for i in range(num_iterations):
        if i == (num_iterations - 1):
            # only display the last iteration
            display = True
        else:
            display = False
        previous_iter_msa, previous_iter_tree = progressive_msa_and_tree(
            sequences=sequences,
            pairwise_aligner=pairwise_aligner,
            metric=metric,
            guide_tree=previous_iter_tree,
            display_aln=display_aln and display,
            display_tree=display_tree and display,
        )

    return previous_iter_msa, previous_iter_tree


# %%
msa, tree = iterative_msa_and_tree(
    sequences=query_sequences,
    pairwise_aligner=global_pairwise_align_nucleotide,
    num_iterations=1,
    display_aln=True,
    display_tree=True,
)

# %%
msa, tree = iterative_msa_and_tree(
    sequences=query_sequences,
    pairwise_aligner=global_pairwise_align_nucleotide,
    num_iterations=2,
    display_aln=True,
    display_tree=True,
)

# %%
msa, tree = iterative_msa_and_tree(
    sequences=query_sequences,
    pairwise_aligner=global_pairwise_align_nucleotide,
    num_iterations=3,
    display_aln=True,
    display_tree=True,
)

# %%
msa, tree = iterative_msa_and_tree(
    sequences=query_sequences,
    pairwise_aligner=global_pairwise_align_nucleotide,
    num_iterations=5,
    display_aln=True,
    display_tree=True,
)

# %% [markdown]
# Some references that I used in assembling these notes include
# [1](http://statweb.stanford.edu/~nzhang/345_web/sequence_slides3.pdf),
# [2](http://math.mit.edu/classes/18.417/Slides/alignment.pdf),
# [3](http://www.sciencedirect.com/science/article/pii/0378111988903307),
# [4](http://bioinformatics.oxfordjournals.org/content/23/21/2947.full),
# [5](http://nar.oxfordjournals.org/content/32/5/1792.full).
