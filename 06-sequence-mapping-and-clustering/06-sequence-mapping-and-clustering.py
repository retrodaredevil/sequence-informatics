# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.1
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# # Reading
# * http://www.scipy-lectures.org/packages/scikit-learn/index.html
# * https://scikit-learn.org/stable/tutorial/index.html
#     * http://scikit-learn.org/stable/tutorial/basic/tutorial.html
#     * http://scikit-learn.org/stable/tutorial/statistical_inference/index.html
#
# # Sequence mapping and clustering
# * A common need in bioinformatics is:
#     * given a number of *query* sequences, to group them based on their similarity:
#         * to one another, or
#         * to sequences in an external reference database, or
#         * to both.
# * The most common way to do this is using sequence alignment (you may be noticing a theme here...).

# %% [markdown]
# ## Sequence mapping
# The process of searching a set of sequences against a reference database to find their best match is typically referred to as **sequence mapping**.
# * One example of this would be in *genome re-sequencing*.
# * If you're searching for polymorphisms in the human genome that may be associated with a phenotype (e.g., a particular disease) you might begin by sequencing the full human genome.
# * Because the human genome has been fully sequenced and (mostly) assembled, you could map your short sequence reads against the full human genome, and then search for loci (or co-located sets of one or more bases) that vary with the phenotype of interest.
# * Because this process is generally performed with DNA sequencing reads, you may also hear it referred to as *read mapping*.
#
# ![](./images/mapping.png)

# %% [markdown]
# A similar process can be applied if there is no reference database to search against.
# * In this case, sequences will be grouped together based on their similarity to one another.
# * This is most often applied to reads of a single gene or locus across the genomes of many different organisms.
# * This process is referred to as **de novo sequence clustering**, and one field where this is common is **microbiomics**, or the study of whole communities of microorganisms.
# * Because we don't know how to culture the vast majority of microbes, most of what we know about the composition of microbial communities is based on sequencing specific marker genes such as the 16S rRNA from all community members.
#     * e.g., in free-living environments, such as the ocean, soil, or surfaces in our homes or offices, or in host-associated environments, such as the human gut.
# * When we obtain a large number of sequence reads, many of the things we want to do with them are too computationally intensive to achieve, such as identifying their taxonomic origin, or understanding where they fall in a phylogenetic tree.
# * So instead, we group sequences that are identical or highly similar in composition into **Operational Taxonomic Units (OTUs)**, and we choose a single representative of that OTU to work with downstream.
# * For example, if we have a group of 16S rRNA reads that are within 97% identity to one member of that cluster (the cluster centroid), then we may assume that the taxonomic origin of the cluster centroid is the same as the taxonomic origin of all of the sequences in the group.
# * This is an *assumption*, which may or may not be true, is a necessary evil given the current technology.
#
# ![](./images/otu.gif)

# %% [markdown]
# ## OTU Clustering
# Another application of grouping similar sequences (or **OTU clustering**, or **OTU picking**, as it is sometimes referred to) is in grouping sequences in a database before investigating them, to reduce taxonomic bias in the database.
# * For example, *E. coli* is one of the most heavily sequenced microbes.
# * If you're interested in understanding the frequency of variants of a specific gene across a range of microbial diversity, you might begin by obtaining all sequences of that gene from [GenBank](https://www.ncbi.nlm.nih.gov/genbank/).
# * Because there may be many more *E. coli* sequences, purely because of sequencing bias, you'd likely want to group your sequences into OTUs before computing variant frequencies, so your calculations are not biased toward the frequencies in *E. coli*, as hundreds of E. coli sequences would likely group to one or a few closely related OTUs.
# * In other words, you're trying to find a **divergent set** of sequences to work with (and an [aptly named tool](http://www.ncbi.nlm.nih.gov/pubmed/16769708) was published in 2006 to automate this process).
#
# We have learned the key tools we need for both sequence mapping and clustering in previous chapters.
# * **Because the process of read mapping is nearly identical to database searching, in this chapter we'll start by exploring how to perform de novo sequence clustering**.
# * At the end of the chapter we'll look at a case where we combine sequence clustering with sequence mapping, which arose to deal with massive sequence datasets generated in microbiomics.

# %% [markdown]
# # What is clustering?
# Grouping things into blobs.
#
# ![](./images/clustering.jpeg)
#
# One type of machine learning...
#
# ## What is machine learning?
# **Machine Learning is about building programs with tunable parameters that are adjusted automatically, so as to improve their behavior by adapting to previously seen data.**
# * Machine Learning can be considered a subfield of Artificial Intelligence since those algorithms can be seen as building blocks to make computers learn to behave more intelligently by somehow generalizing rather that just storing and retrieving data items like a database system would do.
# * In other words, AI is machine learnng wrapped into a bot or autonomous agent.
#
# ### How to format data for machine learning or statistics? (important, key point here)
# * In general, a learning problem considers a set of n samples of data and then tries to predict properties of unknown data.
# * If each sample/feature/observation/etc. is more than a single number, for instance, a multi-dimensional entry (aka multivariate data), it is said to have several attributes or features.
#
# This is very important:
# * Machine learning algorithms, like those implemented in scikit-learn expect data to be stored in a two-dimensional array or matrix.
# * The arrays can be either numpy arrays, or in some cases scipy.sparse matrices.
# * The size of the array is expected to be [rows, cols] = $[n\_samples, n\_features]$
#     * $n\_samples$: The number of samples: each sample is an item to process (e.g. classify, cluster, predict).
#       A sample can be a document, a picture, a sound, a video, an astronomical object, a row in database or CSV file, or whatever you can describe with a fixed set of quantitative traits.
#     * $n\_features$: The number of features or distinct traits that can be used to describe each item in a quantitative manner.
#       Features are generally real-valued, but may be boolean or discrete-valued in some cases.
#         * Most features are used to predict (the data).
#         * Some features are the goal of prediction (class, cluster, continuous valued prediction, etc.).
#
# The number of features must be fixed in advance.
# * However it can be very high dimensional (e.g. millions of features) with most of them being zeros for a given sample.
# * This is a case where scipy.sparse matrices can be useful, in that they are much more memory-efficient than numpy arrays.
#
# One example implementation, Scikit-learn, deals with learning information from one or more datasets that are represented as 2D arrays.
# * They can be understood as a list of multi-dimensional observations.
# * We say that the first axis of these arrays is the samples axis, while the second is the features axis.
#
# This also happen to be the best format for experimental data in organisms/humans:
# * rows - individuals, observations, samples, etc.
# * cols - feature vector with one column per measure

# %% [markdown]
# #### Cahoot-11-1
# https://umsystem.instructure.com/courses/97521/quizzes/292103

# %% [markdown]
# ### Types of learning problem
# * We can separate **learning problems** in a few large categories:
#
# ![](./images/Type-of-Machine-learning.jpg)
#
# ![](./images/Types-of-Learning.png)
#
# ![](./images/types-of-ml.jpg)
#
# * Learning with a teacher:
#     * Reinforcement learning with loose, sparse feedback (not really covered in this class).
#     * **supervised learning**, in which the data comes with additional attributes that we want to predict.
#     This problem can be either:
#         * **classification**: samples belong to two or more classes and we want to learn from already labeled data how to predict the class of unlabeled data.
#             * An example of classification problem would be the handwritten digit recognition example, in which the aim is to assign each input vector to one of a finite number of discrete categories.
#             * Another way to think of classification is as a discrete (as opposed to continuous) form of supervised learning where one has a limited number of categories and for each of the n samples provided, one is to try to label them with the correct category or class.
#         * **regression**: if the desired output consists of one or more continuous variables, then the task is called regression. An example of a regression problem would be the prediction of the length of a salmon as a function of its age and weight.
#
# * **unsupervised learning**, in which the training data consists of a set of input vectors x without any corresponding target values. The goal in such problems may be
#     * to discover groups of similar examples within the data, where it is called **clustering**, or
#     * to determine the distribution of data within the input space, known as **density estimation**, or
#     * to project the data from a high-dimensional space down to two or three dimensions for the purpose of visualization, known as **dimensionaly reduction**.
#
# ![](./images/toml.png)
#
# Which to choose?
# ![0](./images/ml_map.png)

# %% [markdown]
# # Clustering is in the eye of the beholder
#
# ![image0.png](./images/image0.png)

# %% [markdown]
# ![image1.png](./images/image1.png)

# %% [markdown]
# ![image2.png](./images/image2.png)

# %% [markdown]
# ![image3.png](./images/image3.png)
#
# ## In other words, clustering quality and form is determined by the algorithm...
#
#
# #### What types of cluster actually exist?
# ![](./images/cluster_types.png)
#
# #### What types of clustering exist, and what are costs and benefits:
# * Each clustering operation has a prediction error on the examples (assuming you know ground truth).
# * The best clustering is the one that minimizes the error.
#
# ![](./images/types_of_cluster.png)
#
# **types**:
# * In hard clustering each example is placed definitively in a class.
# * In soft clustering each example has a probability distribution over its class.
#
# **Mechanisms**
# * Connectivity: e.g,. builds models based on distance connectivity.
# * Centroid: e.g., k-means algorithm represents each cluster by a single mean vector.
# * Distribution: clusters are modeled using statistical distributions: multivariate normal distributions used by the Expectation-maximization algorithm.
# * Density: e.g,. DBSCAN and OPTICS defines clusters as connected dense regions in the data space.
# * Subspace: in Biclustering (also known as Co-clustering or two-mode-clustering), clusters are modeled with both cluster members and relevant attributes.
# * Group: some algorithms do not provide a refined model for their results and just provide the grouping information.
# * Graph-based: a clique, that is, a subset of nodes in a graph such that every two nodes in the subset are connected by an edge can be considered as a prototypical form of cluster.
#
# **Features**
# * strict partitioning: here each object belongs to exactly one cluster
# * strict partitioning with outliers: objects can also belong to no cluster, and are considered outliers.
# * overlapping: (alternative clustering, multi-view clustering): while usually a hard clustering, objects may belong to more than one cluster.
# * hierarchical: objects that belong to a child cluster also belong to the parent cluster
# * subspace: while an overlapping clustering, within a uniquely defined subspace, clusters are not expected to overlap.

# %% [markdown]
# ## Examples
# <a href="http://scikit-learn.org/stable/modules/clustering.html" class="uri">http://scikit-learn.org/stable/modules/clustering.html</a>
#
# <a href="https://dashee87.github.io/data%20science/general/Clustering-with-Scikit-with-GIFs/" class="uri">https://dashee87.github.io/data%20science/general/Clustering-with-Scikit-with-GIFs/</a>
#
# ![image4.png](./images/image4.png)

# %% [markdown]
# ### K-means
# <a href="https://en.wikipedia.org/wiki/K-means_clustering" class="uri">https://en.wikipedia.org/wiki/K-means_clustering</a>
#
# * In centroid-based clustering, clusters are represented by a central vector, which may not necessarily be a member of the data set.
# * When the number of clusters is fixed to k, k-means clustering gives a formal definition as an optimization problem:
#     * find the k cluster centers and assign the objects to the nearest cluster center, such that the squared distances from the cluster are minimized.
# * The **k-means algorithm** is used for hard clustering.
#
# Inputs
# * Training examples
# * number of classes, k
#
# Outputs
# * a prediction of value for each feature for each class
# * assignment of examples to classes
#
# ![image5.png](./images/image5.png)
#
# 1\) k initial "means" (in this case k=3) are **randomly generated/chosen** within the data domain (shown in color).

# %% [markdown]
# ![image6.png](./images/image6.png)
#
# 2\) k clusters are created by associating every observation with the nearest mean.
# The partitions here represent the Voronoi diagram generated by the means.
#
# ![image7.png](./images/image7.png)
#
# 3\) The centroid of each of the k clusters becomes the new mean.
#
# <a href="https://en.wikipedia.org/wiki/Centroid" class="uri">https://en.wikipedia.org/wiki/Centroid</a>
#
# (the point closet to the mean of x's and y's)
#
# ![image8.png](./images/image8.png)
#
# 4\) Steps 2 and 3 are repeated until convergence has been reached.
# * An assignment of examples to classes is stable if running both the $M$ step and the $E$ step does not change the assignment.
# * This algorithm will eventually converge to a stable local minimum.
# * It is not guaranteed to converge to a global minimum.
# * It is sensitive to the relative scale of the dimensions.
# * Increasing $k$ can always decrease error until $k$ is the number of different examples.
# * The result may depend on the initial clusters. As the algorithm is usually fast, it is common to run it multiple times with different starting conditions.
# * There are many sub-variations in implementation and algorithm, for example, using data points as centroids, instead of just mean points:
#
# https://en.wikipedia.org/wiki/K-medoids (uses actual data points as centers instead of means)

# %% [markdown]
# #### Cahoot-11-2
# https://umsystem.instructure.com/courses/97521/quizzes/292102

# %% [markdown]
# ### Some animated gifs
# <a href="http://shabal.in/visuals/kmeans/left.gif" class="uri">http://shabal.in/visuals/kmeans/left.gif</a>
#
# <a href="https://upload.wikimedia.org/wikipedia/commons/e/ea/K-means_convergence.gif" class="uri">https://upload.wikimedia.org/wikipedia/commons/e/ea/K-means_convergence.gif</a>

# %% [markdown]
# ### An example: k-means
# <a href="http://www.bigendiandata.com/2017-04-18-Jupyter_Customer360/" class="uri">http://www.bigendiandata.com/2017-04-18-Jupyter_Customer360/</a>
#
# ![image9.png](./images/image9.png)
#
# Why question? Why does k-means not suceed in the above image as it should
# =========================================================================

# %% [markdown]
# Situations to use each algorithm
# ================================
# ![image10.png](./images/image10.png)

# %%
import scipy as sp  # type: ignore
import sklearn  # type: ignore
from sklearn import datasets, cluster
import matplotlib.pyplot as plt  # type: ignore
import numpy as np
import networkx as nx  # type: ignore
import pandas as pd  # type: ignore
import seaborn as sns  # type: ignore
import qiime_default_reference  # type: ignore
import random
import functools
import math
import skbio  # type: ignore
import time
from IPython.core import page  # type: ignore
from typing import Callable

page.page = print

# %% [markdown]
# ## De novo clustering of sequences by similarity
# Sequence alignment and clustering can work together, each enabling the other.
#
# * In an ideal world, we would
#     * perform a full multiple sequence alignment of all of our sequences,
#     * compute their pairwise similarities (or dissimilarities), and
#     * use those values to group sequences that are above some *similarity threshold* into *OTU clusters* (just *OTUs* from here).
# * However, as we discussed in the multiple sequence alignment chapter, that is infeasible for more than a few tens of sequences due to computational and memory requirements.
# * Even progressive alignment can't typically handle more than a few tens of thousands of sequences (at least with the currently available implementations, that I am aware of), so OTU clustering is generally achieved by picking pairs of sequences to align.
# * You'll notice in this section that many of the heuristics that have been applied for speeding up database searching are similar to the heuristics applied for OTU clustering.
# * We'll work with [SSW](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0082138) for Smith-Waterman pairwise alignment with affine gap scoring here, though in principle any pairwise aligner could be substituted.
#
# Let's define a collection of sequences to work with.
# * These are derived from the [Greengenes](http://greengenes.secondgenome.com/) [13_8](ftp://greengenes.microbio.me/greengenes_release/gg_13_5/) database, and we're pulling them from the [QIIME default reference project](https://github.com/biocore/qiime-default-reference).
# * We can load these as a list of sequences using ``skbio.parse.sequences.parse_fasta``, and count them by taking the length of the list.
# * For the sake of runtime, we'll work with only a small random subset these sequences.
#
# **Our goal here will be to group these sequences into OTUs based on some similarity threshold that we define.**
# * If we set this similarity threshold at 70%, meaning that the sequences within that OTU are 70% identical (either to each other, or maybe to some representative of that cluster - we'll explore some variants on that definition below), we would call these *70% OTUs*.

# %%
seqs_16s = []
fraction_to_keep = 0.001

for e in skbio.io.read(
    file=qiime_default_reference.get_reference_sequences(),
    format="fasta",
    constructor=skbio.DNA,
):
    if random.random() < fraction_to_keep:
        seqs_16s.append(e)

print(len(seqs_16s), "sequences imported.\n")
print(seqs_16s[1])
seqs_16s[1]

# %% [markdown]
# ## How do we define sequence clusters?
# Consider these concepts, sequentially building:
# ![](./images/cluster-legend.png)

# %% [markdown]
# ## Three types we will implement now
#
# ![](./images/cluster-types.png)
#
# Furthest neighbor coming up first):

# %% [markdown]
# ### Furthest neighbor clustering
# The first approach we'll look at is one that has been called *furthest neighbor*, because whether a sequence becomes a member of a cluster is defined by it's most dissimilar (furthest) "neighbor" in that cluster.
#
# The way this algorithm works is that we start with our list of sequences.
# * Because this is *de novo* clustering, by definition our first sequence is added to a new cluster (because there are no pre-existing clusters).
# * We'll call this `OTU 1`.
# * We then iterate over the remaining sequences.
# * For the second sequence we compute its pairwise alignment with the first, followed by computing their percent similarity.
# * If their percent similarity is greater than or equal to the similarity threshold, we add the second sequence to `OTU 1`.
# * If is it less than the similarity threshold, we create a new OTU, `OTU 2`, and add the second sequence to that cluster.
#
# We continue iterating over the remaining sequences.
# * For each sequence, we compute the pairwise similarity between that sequence and all sequences in each OTU.
# * **If the percent similarity between a query sequence and *all* sequences in a given OTU (say OTU $x$ is greater than the similarity threshold, we add the sequence to OTU $x$.)**
# * Otherwise we check the next OTU in the list.
# * If this criteria is not met for any of the OTUs, then we define a new OTU to add the sequence to.
#
# Let's implement this, and then try it out on some test sequences.
#
# The general clustering wrapper for each method:

# %%
def cluster(
    seqs: list[skbio.Sequence],
    similarity_threshold: float,
    cluster_fn: Callable[
        [
            skbio.Sequence,
            nx.Graph,
            float,
            Callable[[skbio.Sequence, skbio.Sequence], skbio.TabularMSA],
            bool,
        ],
        tuple[bool, list[tuple[nx.Graph, float]]],
    ],
    aligner: Callable[
        [skbio.Sequence, skbio.Sequence], skbio.TabularMSA
    ] = skbio.alignment.local_pairwise_align_ssw,
    verbose: bool = False,
) -> tuple[list[nx.Graph], int]:
    """
    Clusters a list of bio sequences into a networkx graph.
    """
    clusters: list[nx.Graph] = []
    num_alignments = 0

    for query_seq in seqs:
        if verbose:
            print(query_seq.metadata["id"])
        clustered = False

        for i, cluster in enumerate(clusters, start=1):
            if verbose:
                print(" OTU %d" % i)
            clustered, alignment_results = cluster_fn(
                seq=query_seq,
                cluster=cluster,
                similarity_threshold=similarity_threshold,
                aligner=aligner,
                verbose=verbose,
            )
            num_alignments += len(alignment_results)
            if clustered:
                break

        if clustered:
            for n, s in alignment_results:
                cluster.add_node(query_seq.metadata["id"], seq=query_seq)
                cluster.add_edge(query_seq.metadata["id"], n, percent_similarity=s)
                # this is very inefficient, but need a way to retain order
                # for centroid clustering.
                # will come back to this...
                cluster.graph["node-order"].append(query_seq.metadata["id"])
            if verbose:
                print("Added to OTU")
        else:
            # create a new cluster containing only this node
            new_cluster = nx.Graph(id=f"OTU {len(clusters) + 1}")
            new_cluster.add_node(query_seq.metadata["id"], seq=query_seq)
            # this is very inefficient, but need a way to retain order
            # for centroid clustering.
            # will come back to this...
            new_cluster.graph["node-order"] = [query_seq.metadata["id"]]
            clusters.append(new_cluster)
            if verbose:
                print("Created OTU")
    return clusters, num_alignments


# %%
def furthest_neighbor(
    seq: skbio.Sequence,
    cluster: nx.Graph,
    similarity_threshold: float,
    aligner: Callable[[skbio.Sequence, skbio.Sequence], skbio.TabularMSA],
    verbose: bool = False,
) -> tuple[bool, list[tuple[nx.Graph, float]]]:
    """
    Decides whether to add a new sequnece to an existing cluster,
    and if so, does adds the sequence to a cluster.

    If new seq similar enough to all members.
    """
    alignment_results = []
    for node in cluster.nodes():
        # aligner takes two sequences:
        aln, _, _ = aligner(seq, cluster.nodes[node]["seq"])
        percent_similarity = 1.0 - aln[0].distance(aln[1])
        alignment_results.append((node, percent_similarity))
        if verbose:
            print("  ", node, percent_similarity)
        if percent_similarity < similarity_threshold:
            return False, alignment_results

    return True, alignment_results


# %%
def show_clusters(
    clusters: list[nx.Graph],
    print_clusters: bool = True,
    plot_clusters: bool = True,
    plot_labels: bool = False,
) -> None:
    """
    Plots the graph using nx and plt.
    """
    G = nx.Graph()

    for c in clusters:
        G = nx.union(G, c)
        if print_clusters:
            print("%s: %s" % (c.graph["id"], [s for s in c.graph["node-order"]]))
    if plot_clusters:
        pos = nx.spring_layout(G)
        nx.draw_networkx_nodes(G, pos, node_color="w")
        # nx.draw_networkx_nodes(G, pos, node_color="w", node_size=1000)
        if G.number_of_edges() > 0:
            # nx.draw_networkx_edges(G, pos, width=3)
            nx.draw_networkx_edges(G, pos)
        if plot_labels:
            nx.draw_networkx_labels(
                G,
                pos,
                labels={d: G.nodes[d]["seq"].metadata["id"] for d in G.nodes()},
                # font_size=30,
            )
        _ = plt.axis("off")
        # plt.rcParams["figure.figsize"] = (10, 10)


# %%
# For our toy example, we want our sequences to align from beginning to end
# so we'll penalize terminal gaps.
global_pairwise_align_nucleotide = functools.partial(
    skbio.alignment.global_pairwise_align_nucleotide, penalize_terminal_gaps=True
)

# %%
s1 = skbio.DNA(sequence="AAAAAAAAAA", metadata={"id": "s1"})
s2 = skbio.DNA(sequence="AAAAATTTTT", metadata={"id": "s2"})
s3 = skbio.DNA(sequence="AAAAAAACCA", metadata={"id": "s3"})
s4 = skbio.DNA(sequence="CCCCAATTTT", metadata={"id": "s4"})
s5 = skbio.DNA(sequence="ACCAAATTTT", metadata={"id": "s5"})
s6 = skbio.DNA(sequence="AGGAAAAAAA", metadata={"id": "s6"})

aln1 = skbio.TabularMSA(sequences=[s1, s2, s3, s4, s5, s6])

print(aln1)

# %% [markdown]
# * Our first sequence, ``s1``, will define a new OTU.
# * We'll call that OTU ``OTU 1``.
# * Our second sequence, ``s2`` falls outside of the similarity threshold to ``S1``, it will also define a new OTU.
# * We'll call that OTU ``OTU 2``.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2],
    similarity_threshold=0.70,
    cluster_fn=furthest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

# %%
print(clusters, "\n")
print(type(clusters), "\n")
print(type(clusters[0]), "\n")
print(num_alignments)

# %% [markdown]
# Figures:
# * In the figures that follow:
#     * points (or nodes) represent sequences, and
#     * line (or edges) connecting points indicate that a pair of sequences are within a defined percent identity threshold.
# * These illustrations are used to describe OTUs, such that a set of points that are connected either directly or indirectly represent a grouping of sequences into an OTU.
# * In this and many cases, the "layout" of a graph is not related to distances or structures.
#     * Specifically, nodes and edges are put somewhere on the page, so that they fit, but not to represent any actual information.

# %%
show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Now imagine that our third sequence, ``s3`` falls within the range of ``s1``.
# * We would cluster ``s3`` into ``OTU 1`` since it is within the similarity range of all of the sequences in ``OTU 1`` (for now that's just ``s1``).
# * We now have three sequences clustered into two OTUs.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3],
    similarity_threshold=0.70,
    cluster_fn=furthest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Now let's cluster a fourth sequence, ``s4``.
# * We find that this falls outside the range of ``OTU 1``, and also outside the range of ``OTU 2``.
# * So, we would create a new OTU, ``OTU 3``, containing ``s4``.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4],
    similarity_threshold=0.70,
    cluster_fn=furthest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Next, let's cluster our fifth sequence, ``s5``.
# * We find that this falls outside the range of ``OTU 1``, but inside the range of both ``OTU 2`` and ``OTU 3``.
# * Algorithmically, we now have a choice to make.
# * How do we decide which OTU a sequence should belong to if it is within the similarity range of several OTUs?
#
# A few choices would be:
# 1. add it to the cluster where it first matches all sequences,
# 2. add it to the cluster where it has the smallest average distance to the cluster members,
# 3. or add it to the cluster with the most members.
#
# \#\#\# Why question: What are some pros an cons of each option?
#
# * There are many other options as well.
# * Let's choose the option 1 here, as it requires performing the fewest number of alignments, so should be fastest (we'll explore that in more detail soon).
# * Our mapping of OTUs to sequences would look like:

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4, s5],
    similarity_threshold=0.70,
    cluster_fn=furthest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Finally, let's cluster our last sequence, ``s6``.
# * In this case, it falls within the similarity range of ``s1``, but outside of the similarity range of ``s3``.
# * So, because our algorithm requires that a sequence be within the similarity range of all sequences in an OTU, ``s6`` cannot be a member of ``OTU 1``, so instead it's assigned to a new OTU, ``OTU 4``.
# * Our final mapping of OTUs to sequences would look like:

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4, s5, s6],
    similarity_threshold=0.70,
    cluster_fn=furthest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# Let's apply that function to our real sequence collection.

# %%
clusters, num_alignments = cluster(
    seqs=seqs_16s, similarity_threshold=0.70, cluster_fn=furthest_neighbor
)

show_clusters(clusters=clusters, plot_clusters=False, plot_labels=False)

# %%
show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# Let's define a function that will be useful for exploring different clustering algorithms:

# %%
def evaluate_cluster_fn(
    cluster_fn: Callable[
        [
            skbio.Sequence,
            nx.Graph,
            float,
            Callable[[skbio.Sequence, skbio.Sequence], skbio.TabularMSA],
            bool,
        ],
        tuple[bool, list[tuple[nx.Graph, float]]],
    ],
    seqs: list[skbio.Sequence],
    similarity_threshold: float,
    display: bool = True,
) -> tuple[int, float, int]:
    """
    Provides some basic statistics about the clustering operation:
    tuple[num_alignments, run_time, and num_clusters]
    """
    start_time = time.time()
    clusters, num_alignments = cluster(
        seqs=seqs, similarity_threshold=similarity_threshold, cluster_fn=cluster_fn
    )
    end_time = time.time()
    run_time = end_time - start_time
    num_clusters = len(clusters)

    if display:
        print("Number of alignments performed: %d" % num_alignments)
        print("Runtime: %1.3fs" % run_time)
        print("Number of clusters: %d" % num_clusters)
        print("Clusters:")
        show_clusters(clusters=clusters, plot_clusters=False, plot_labels=False)
    return num_alignments, run_time, num_clusters


# %% [markdown]
# Now let's apply that:

# %%
r = evaluate_cluster_fn(
    cluster_fn=furthest_neighbor, seqs=seqs_16s, similarity_threshold=0.70, display=True
)


# %% [markdown]
# ### Nearest neighbor clustering
# Let's try a variant on this algorithm.
# * How would things change if **instead of requiring that a sequence be within the similarity threshold of all sequences in an OTU, we only required that it be within the similarity threshold of one sequence in that OTU**?
# * This is referred to as **nearest neighbor** clustering, because cluster membership is defined by the percent similarity to the most similar (or *nearest*) "neighbor" in the cluster.
# * Let's implement nearest neighbor clustering and look at the same six toy sequences as above.
#
# <div style="float: left; margin-left: 30px;"><img style="float: left; margin-left: 30px;" src="https://raw.githubusercontent.com/gregcaporaso/An-Introduction-To-Applied-Bioinformatics/master/book/fundamentals/images/cluster-types.png" align=left></div>

# %%
def nearest_neighbor(
    seq: skbio.Sequence,
    cluster: nx.Graph,
    similarity_threshold: float,
    aligner: Callable[[skbio.Sequence, skbio.Sequence], skbio.TabularMSA],
    verbose: bool = False,
) -> tuple[bool, list[tuple[nx.Graph, float]]]:
    """
    Decides whether to add a new sequnece to an existing cluster,
    and if so, does adds the sequence to a cluster.

    If new seq similar enough to at least one member.
    """
    alignment_results = []

    for node in cluster.nodes():
        aln, _, _ = aligner(seq, cluster.nodes[node]["seq"])
        percent_similarity = 1.0 - aln[0].distance(aln[1])
        alignment_results.append((node, percent_similarity))
        if verbose:
            print(" ", node, percent_similarity)
        if similarity_threshold <= percent_similarity:
            return True, alignment_results
    return False, alignment_results


# %%
# Reminder:
print(aln1)

# %% [markdown]
# * Our first sequence, ``s1``, will again define a new OTU, ``OTU 1``.
# * Our second sequence, ``s2``, still falls outside of the similarity threshold to ``s1``, so will define ``OTU 2``.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2],
    similarity_threshold=0.70,
    cluster_fn=nearest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Now imagine that our third sequence, $S3$ falls within the range of $OTU1$.
# * We would cluster $S3$ into $OTU1$ with $S1$.
# * We now have three sequences clustered into two OTUs.
# * So far, things are looking the same as before, except notice how our OTU definition (grey shading) is now different.
# * Because any sequence within the similarity threshold of *any* of sequence in the OTU will fall into this OTU, the shading now covers the area covered by either of our sequences, rather than the area covered by both of our sequences (in set theory terminology, it is the *union* now, where previously it was the *intersection*).

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3],
    similarity_threshold=0.70,
    cluster_fn=nearest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Now let's cluster a fourth sequence, $S4$.
# * We find that this falls outside the range of $OTU1$, and also outside the range of $OTU2$.
# * So, we would create a new OTU, $OTU3$, containing $S4$.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4],
    similarity_threshold=0.70,
    cluster_fn=nearest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Next, let's cluster our fifth sequence, $S5$.
# * We find that this falls outside the range of $OTU1$, and inside the range of both $OTU2$ and $OTU3$.
# * As with furthest neighbor, we have a choice of how to handle this.
# * We'll again assign it to the first cluster that it match.
# * Our mapping of OTUs to sequences would look like:

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4, s5],
    similarity_threshold=0.70,
    cluster_fn=nearest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Finally, let's cluster our last sequence, $S6$.
# * Remember that $S6$ falls within the similarity range of $S1$, but outside of the similarity range of $S3$.
# * In furthest neighbor, this meant that it was assigned to a new OTU, but with nearest neighbor it meets the inclusion criteria for $OTU1$.
# * So, our final mapping of OTUs to sequences would look like:

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4, s5, s6],
    similarity_threshold=0.70,
    cluster_fn=nearest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# **One feature that becomes obvious here is the order dependence of these methods.**
# * If sequences are provided in different order across different clustering runs, the cluster definitions will change.
# * For example, how would the results differ if the sequences were processed in this order: $S1$, $S3$, $S4$, $S5$, $S6$, $S2$?

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s3, s4, s5, s6, s2],
    similarity_threshold=0.70,
    cluster_fn=nearest_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# Finally, let's apply this to our collection of real sequences again.

# %%
r = evaluate_cluster_fn(
    cluster_fn=nearest_neighbor, seqs=seqs_16s, similarity_threshold=0.70
)


# %% [markdown]
# * You'll notice that both the runtime and the number of alignments performed here are different.
# * Most of the runtime is spent aligning, so runtime and number of alignments should be strongly correlated.
# * There was another affect here though:
#     * we have a different number of OTUs.
#     * Is this result better or worse?
# * There is not a definitive answer to that question:
#     * it really depends on the application
#     * what we would ultimately want to know is how does that affect our ability to interpret the data.
# * Remember: OTU clustering is a necessary evil to deal with the massive amounts of data that we have.
# * We don't necessarily care about things like how many OTUs a method gives us, but rather how the clustering process helps or hurts us in answering the biological questions driving the analysis.
# * We'll explore this concept more in later chapters, but it is an important one that algorithm developers sometimes lose track of.

# %% [markdown]
# #### Cahoot-11-3
# https://umsystem.instructure.com/courses/97521/quizzes/292480

# %% [markdown]
# ### Centroid clustering
# * So, given that the number of alignments performed is correlated with runtime, are there ways that we can reduce the number of alignments that are computed by a clustering algorithm? One approach for that is generally referred to as **centroid clustering**.
# * Here, we can say that **a sequence is assigned to an OTU if it is within the similarity threshold of the first sequence in that OTU**.
# * The first sequence in that cluster then becomes the *cluster centroid*:
#     * cluster membership is defined by similarity to that one particular sequence, which effectively sits at the "center" of that OTU.
# * Let's implement this and apply the process to our six sequences.
#
# <div style="float: left; margin-left: 30px;"><img style="float: left; margin-left: 30px;" src="https://raw.githubusercontent.com/gregcaporaso/An-Introduction-To-Applied-Bioinformatics/master/book/fundamentals/images/cluster-types.png" align=left></div>

# %%
def centroid_neighbor(
    seq: skbio.Sequence,
    cluster: nx.Graph,
    similarity_threshold: float,
    aligner: Callable[[skbio.Sequence, skbio.Sequence], skbio.TabularMSA],
    verbose: bool = False,
) -> tuple[bool, list[tuple[nx.Graph, float]]]:
    """
    Decides whether to add a new sequnece to an existing cluster,
    and if so, does adds the sequence to a cluster.

    If new seq similar enough to actual prototypical datapoint.
    """
    alignment_results = []
    centroid_node = cluster.graph["node-order"][0]
    aln, _, _ = aligner(seq, cluster.nodes[centroid_node]["seq"])
    percent_similarity = 1.0 - aln[0].distance(aln[1])
    if verbose:
        print(" ", centroid_node, percent_similarity)
    alignment_results.append((centroid_node, percent_similarity))
    return similarity_threshold <= percent_similarity, alignment_results


# %% [markdown]
# ``s1`` will again define ``OTU 1`` and ``s2`` still falls outside of the similarity threshold to ``s1``, so will define ``OTU 2``.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2],
    similarity_threshold=0.70,
    cluster_fn=centroid_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Next, ``s3`` falls within the range of ``OTU 1``.
# * We would cluster ``s3`` into ``OTU 1`` with ``s1``, and now have three sequences clustered into two OTUs.
# * Again, our sequence to OTU mapping looks the same as before at this stage.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3],
    similarity_threshold=0.70,
    cluster_fn=centroid_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Now let's cluster a fourth sequence, $S4$.
# * We find that this falls outside the range of $OTU1$, and (just barely) outside the range of $OTU2$.
# * So, we would create a new OTU, $OTU3$, containing $S4$.

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4],
    similarity_threshold=0.70,
    cluster_fn=centroid_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Next, let's cluster our fifth sequence, $S5$.
# * We find that this falls outside the range of $OTU1$, and inside the range of both $OTU2$ and $OTU3$.
# * As with furthest neighbor, we have a choice of how to handle this.
# * We'll again assign it to the cluster to which it is the most similar.
# * Our mapping of OTUs to sequences would look like:

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4, s5],
    similarity_threshold=0.70,
    cluster_fn=centroid_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Finally, let's cluster our last sequence, ``s6``.
# * Remember that ``s6`` falls within the similarity range of ``s1``, but outside of the similarity range of ``s3``.
# * In furthest neighbor, this meant that it was assigned to a new OTU;
#     * in nearest neighbor, it was assigned to ``OTU 1``, and that is what happens here.
# * Our final mapping of OTUs to sequences would look like:

# %%
clusters, num_alignments = cluster(
    seqs=[s1, s2, s3, s4, s5, s6],
    similarity_threshold=0.70,
    cluster_fn=centroid_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %% [markdown]
# * Again, let's think about order dependence here.
# * How would this differ if ``s3`` was the centroid of ``OTU 1``, rather than ``s1``?

# %%
clusters, num_alignments = cluster(
    seqs=[s3, s1, s2, s4, s5, s6],
    similarity_threshold=0.70,
    cluster_fn=centroid_neighbor,
    aligner=global_pairwise_align_nucleotide,
    verbose=True,
)

show_clusters(clusters=clusters, plot_labels=True)

# %%
r = evaluate_cluster_fn(
    cluster_fn=centroid_neighbor, seqs=seqs_16s, similarity_threshold=0.70
)

# %% [markdown]
# * We've now reduced the number of alignments and the runtime.
# * What was the effect on the results?
#
# Some questions?
# * How is this similar to k-means?
# * Is it likely better or worse?
# * Why not just perform k-means?
# * What heuristic could we use to pick an improved centroid without alignments?
# * Can we improve the initial centroid selection?
# * What happens if we re-pick centroids?

# %% [markdown]
# ### Three different definitions of OTUs
# * With these three algorithms, we've looked at three different ways of defining a cluster or OTUs.
# * This figure illustrates the differences in each definition, where the points illustrate sequences, the solid lines illustrated a similarity threshold, and the grey areas represent the space defining an OTU (or where a new sequence must fall to be considered part of an OTU).
#
# ![](./images/cluster-legend.png)
#
# ![](./images/cluster-types.png)
#
# #### furthest neighbor
# where membership in a cluster is defined by a query sequence's distance to the most dissimilar sequence already in the cluster (its furthest neighbor), the definition of the cluster shrinks with additional sequences.
# * This can have some undesired effects, such as the definition of a cluster becoming so restrictive that it is unlikely that new sequences will ever be added.
#
# #### nearest neighbor
# where membership in a cluster is defined by a query sequence's distance to the most similar sequence already in the cluster (its nearest neighbor), the definition of the cluster can grow with additional sequences.
# * This can have some undesired effects as well:
#     * in the worst case we could end up with one single cluster that contains all of our sequences.
#
# #### centroid distance
# where membership in a cluster is defined by a query sequence's distance to the cluster's centroid sequence, the size of the cluster always remains the same, but the role that the first sequence added to a cluster plays becomes much more important.
# * So, it's very important that the cluster centroids are well-chosen.
# * One undesired effect of centroid distance cluster is that the cluster sizes are fixed, which may or may not always make biological sense (for example, if the marker gene evolves at a faster rate in some taxa than others, that can't be reflected in the cluster definitions.
#
# ### Summary
# All of these methods have good features and bad features, and that in fact is a common feature of heuristics (if they were perfect, they wouldn't be heuristics after all...).

# %% [markdown]
# ## Comparing properties of our clustering algorithms
# * We so far looked at these algorithms based on a single similarity threshold and a single sequence collection, but as we know from previous chapters it's important to know how features such as run time change with different inputs.
# * Let's explore these algorithms in the context of changing sequence collection sizes and similarity thresholds.
# * To do this, we're going to do a *parameter sweep*.
# * We're going to "sweep" over a bunch of different input parameters, and then explore how they affect our results.
#
# The parameters that we'll sweep over here are:
#  * input sequences
#  * similarity thresholds for sequence clustering
#  * clustering methods
#  * and number of sequences.
#
# The outputs that we'll explore are:
#  * number of alignments that were performed
#  * run time
#  * and number of resulting OTUs (or clusters).
#
# For the sake of runtime, I'm only looking at few settings for each of the input parameters.
# * You may want to expand from there, but note that the number of combinations grows quickly since we're going to analyze all combinations of the parameters.
# * For example, for our similarity threshold sweep, if we test two sequence collections, three similarity thresholds, and three clustering methods, we would run $2 \times 3 \times 3 = 18$ clustering runs.
# * If we add one more similarity threshold, that number would jump to $2 \times 4 \times 3 = 24$ clustering runs.
# * So, these numbers can increase quickly.

# %%
n = 2
fraction_to_keep = 0.002
similarity_thresholds = [0.60, 0.70, 0.80]
cluster_fns = [
    ("furthest neighbor", furthest_neighbor),
    ("nearest neighbor", nearest_neighbor),
    ("centroid neighbor", centroid_neighbor),
]
sizes = [20, 30, 45]


def get_random_sequence_collection(
    input_seqs: list[skbio.Sequence], fraction_to_keep: float
) -> list[skbio.Sequence]:
    "Gets a statistically random sample of a list of bio sequences."
    result = []
    for e in input_seqs:
        if random.random() < fraction_to_keep:
            result.append(e)
    return result


data = []

# first, sweep over different random sequence collections
for i in range(n):
    random_sc = get_random_sequence_collection(
        input_seqs=skbio.io.read(
            qiime_default_reference.get_reference_sequences(),
            format="fasta",
            constructor=skbio.DNA,
        ),
        fraction_to_keep=fraction_to_keep,
    )

    # then, sweep over clustering functions
    for cluster_fn in cluster_fns:
        # then, sweep over data set sizes
        similarity_threshold = 0.70

        for size in sizes:
            current_sc = random_sc[:size]
            alignment_count, run_time, cluster_count = evaluate_cluster_fn(
                cluster_fn=cluster_fn[1],
                seqs=current_sc,
                similarity_threshold=similarity_threshold,
                display=False,
            )
            current_result = [
                cluster_fn[0],
                size,
                alignment_count,
                run_time,
                cluster_count,
                similarity_threshold,
            ]
            data.append(current_result)

        current_sc = random_sc[: sizes[-1]]

        # finally, sweep over similarity thresholds
        for similarity_threshold in similarity_thresholds:
            alignment_count, run_time, cluster_count = evaluate_cluster_fn(
                cluster_fn=cluster_fn[1],
                seqs=current_sc,
                similarity_threshold=similarity_threshold,
                display=False,
            )
            current_result = [
                cluster_fn[0],
                size,
                alignment_count,
                run_time,
                cluster_count,
                similarity_threshold,
            ]
            data.append(current_result)

df = pd.DataFrame(
    data,
    columns=[
        "Cluster method",
        "Number of sequences",
        "Number of alignments",
        "Run time (s)",
        "Number of clusters",
        "Similarity threshold",
    ],
)

# %% [markdown]
# * Remember that above I said that most of the time in each of these clustering algorithms is spent doing pairwise alignment.
# * Let's plot the run time of each clustering method as a function of the number of alignments computed in the cluster process so I can prove that to you.

# %%
sns.set(font_scale=2)

r, p = sp.stats.pearsonr(x=df["Number of alignments"], y=df["Run time (s)"])

print(f"Pearson r: {r:1.4f}")
print(f"Pearson p-value: {p:1.4e}")

sns.lmplot(x="Number of alignments", y="Run time (s)", data=df, size=8)

# %% [markdown]
# Next, we can see how each of these methods scale with the similarity threshold.

# %%
g = sns.lmplot(
    x="Similarity threshold",
    y="Run time (s)",
    data=df[df["Number of sequences"] == max(sizes)],
    hue="Cluster method",
    size=8,
)

# %% [markdown]
# Next let's look at run time as a function of the number of sequences to be clustered.

# %%
g = sns.lmplot(
    x="Number of sequences",
    y="Run time (s)",
    data=df[df["Similarity threshold"] == 0.70],
    hue="Cluster method",
    size=8,
)

# %% [markdown]
# * **Which of these methods do you think will scale best** to continuously increasing numbers of sequences (e.g., as is currently the trend in microbiomics)?
# * Finally, let's look at the number of clusters (or OTUs) that are generated with each method at each similarity threshold.

# %%
# New is this:
g = sns.catplot(
    x="Similarity threshold",
    y="Number of clusters",
    data=df[df["Number of sequences"] == max(sizes)],
    hue="Cluster method",
    kind="bar",
)

# %% [markdown]
# ## Reference-based clustering to assist with parallelization
# * Up until this point we have focused our discussion on *de novo* OTU clustering, meaning that sequences are clustered only against each other, with no external reference.
# * This is a very widely applied protocol, and the primary function of popular bioinformatics tools such as [CD-HIT](http://bioinformatics.oxfordjournals.org/content/28/23/3150.long) and [UCLUST](http://bioinformatics.oxfordjournals.org/content/26/19/2460.long).
# * Another category of OTU clustering protocols is also popular however: reference-based OTU clustering, where a external reference database of sequences is used to aid in cluster definition.
#
# Reference-based clustering is typically a centroid-based approach, where cluster centroids are pre-defined based on sequences in a database.
# * From here, reference-based clustering is performed in one of two ways.
# * In *closed-reference* OTU clustering, the set of centroids is static, and sequences that don't match a centroid are not clustered.
# * In *open-reference* OTU clustering, the set of centroids can expand: sequences that don't match an existing centroid can become new centroid sequences.
#
# Please note, OTU picking and OTU clustering are synonymous.
#
# ### When you know ground truth:
# how do you evaluate the performance of a clustering algorithm?
#
# <a href="https://en.wikipedia.org/wiki/Rand_index" class="uri">https://en.wikipedia.org/wiki/Rand_index</a>

# %% [markdown]
# Iris data set again
# ===================
# ![image11.png](./images/image11.png)
#
# Example: remember the iris dataset?
#
# K-means on iris:
# https://scikit-learn.org/stable/auto_examples/cluster/plot_cluster_iris.html
#
# ## Why Q:
# * If we squash purple and yellow into 2D, are they still distinguishable?
# * What happens if you have more than 3D and need to visualize the custers?
# * Can we? How?
# * Can we validate the category visually?
#
# Inferred clusters:
# ![image12.png](./images/image12.png)
#
# Actual clusters:
# ![image13.png](./images/image13.png)
#
# We can see the separation in higher D space, but not if it's squashed (1D is simply eliminated).

# %%
iris = sklearn.datasets.load_iris()
print(iris.data)
iris.data

# %%
X_iris = iris.data
print(X_iris)
X_iris

# %%
y_iris = iris.target
print(y_iris)
y_iris

# %%
# Instantiate the model
k_means = sklearn.cluster.KMeans(n_clusters=3)

# Train the model
k_means.fit(X_iris)

print(k_means.labels_[::10])

print(y_iris[::10])

# %% [markdown]
# Self-organizing maps
# ====================
# a.k.a. Kohonen self-organizing maps
#
# ## For a given algorithm, there is likely a neural network variant that can solve its problem better...
#
# * https://en.wikipedia.org/wiki/Self-organizing_map
# * http://www.ai-junkie.com/ann/som/som1.html
# * https://visualstudiomagazine.com/Articles/2019/01/01/self-organizing-maps-python.aspx?Page=2
# * https://github.com/JRC1995/Self-Organizing-Map

# %% [markdown]
# ### Example structure:
# Each data point has 2 input features (x, y), and there are 16 nodes (think of these as data-points for now, though they aren't really).
#
# * Very small 2D SOM Kohonen network of 4 X 4 lattice of nodes each of which is connected to the input layer (shown in green) representing a two dimensional vector.
# * Lines connecting nodes only represent adjacency, and do not signify a connection as normally in a neural network.
# * There are no lateral connections between nodes within the lattice.
#
# ![image17.png](./images/image17.png)
#
# * Each node has a specific topological position (an x, y coordinate in the lattice) and contains a vector of weights of the same dimension as the input vectors.
# * If the training data consists of vectors, V, of n dimensions:
#
#     $V_1, V_2, V_3...V_n$
#
# * Then each node will contain a corresponding weight vector W, of n dimensions:
#
#     $W_1, W_2, W_3...W_n$

# %% [markdown]
# ### Overview
#
# #### Two phases
# 1. Training (fitting lattice-map to data)
#
# ![](./images/Somtraining.png)
#
# 2. Mapping (mapping each data point to a node)
#
# #### Parts and pieces
# * Goal is to define a set of centroids, and to assign each object in a data set to the centroid that provides the best approximation of that object, while updating those cenroids to fit the data better.
# * One neuron associated with each centroid.
# * As with incremental k-means, data objects are processed one at a time, and the closest centroid(s) is updated.
# * Unlike k-means, SOM impose a topographic ordering on the centroids, and nearby centroids are also updated.
# * The processing of points continues until some predetermined limit is reached, or until the centroids cease to change above some threshold.
# * The final output of the SOM technique is a set of centroids that implicitly define potential clusters, if thresholded.
# * Each cluster consists of the points closest to a particular centroid.
# * SOM enforces neighborhood relationships on the resulting cluster centroids.
#     * So clusters that are neighbors are more closely related to one another, than clusters that are not.
# * SOM may be considered a nonlinear generalization of Principal components analysis (PCA), which we will cover in the next section of the class (applications of diversity informatics). SOM is often better than PCA...

# %% [markdown]
# ### Example dataset and structure:
# These are colors
# ![image14.png](./images/image14.png)
#
# Colors are encoded as a `tuple[int, int, int]` per pixel.
# Below are some random RBG values (3D) stuck into a 2D matrix:
#
# ![image15.png](./images/image15.png)
#
# How might we shuffle the 2D matrix such that like colors were near each other?
#
# SOMs map higher D to lower D, and implicitly cluster.
# In this case, color is 3D, and we want to group in 2D.
#
# ![image16.png](./images/image16.png)
#
# * This SOM has a default lattice size of 40 X 40.
# * Each node in the lattice has **three weights**
#     * one for each element of the input vector.
#     * **red, green and blue** (which are just integer pixel values).
# * Each node is represented by a rectangular cell
#
# ![image18.png](./images/image18.png)

# %% [markdown]
# ### SOM learning principles
# * SOM does not need a target output to be specified.
#     * It is an unsupervised learning algorithm.
# * Where the node weights match the input vector, that area of the lattice is selectively optimized, to more closely resemble the data for the class of that input vector.
# * From an initial distribution of random weights, over many iterations, the SOM eventually settles into a map of stable zones.
# * Each zone is effectively a feature classifier, so you can think of the graphical output as a type of feature map of the input space
#     * e.g,. the blocks of similar colour represent the individual zones.
# * Any new, previously unseen input vectors presented to the network will stimulate nodes in the zone with similar weight vectors.

# %% [markdown]
# ### Several steps over many iterations:
# 1. Each node's weights are initialized.
#     * Random initial weights are good for non-linear complex data.
# 2. A vector is chosen at random from the set of training data and presented to the lattice.
# 3. Every node is examined to calculate which nodes's weights are most like the input vector.
#     * The winning node is commonly known as the Best Matching Unit (BMU).
# 4. The radius of the neighbourhood of the BMU is now calculated.
#     * This is a value that starts large, typically set to the 'radius' of the lattice, but diminishes each time-step.
#     * Any nodes found within this radius are deemed to be inside the BMU's neighbourhood.
# 5. From the nodes found in step 4, each neighbouring node's weights are adjusted to make them more like the input vector.
#     * The closer a node is to the BMU, the more its weights get altered.
# 6. Repeat from step 2 onward, for N iterations.

# %% [markdown]
# #### Step 3 expanded (BMU):
# * To determine the best matching unit, one method is to iterate through all the nodes and calculate the Euclidean distance between each node's weight vector and the current input vector.
# * The node with a weight vector closest to the input vector is tagged as the BMU.
# * The Euclidean distance is given as:
#
# $dist=\sqrt{\sum_{i=o}^n (V_i-W_1)^2}$
#
# where
# * V is the current input vector
# * W is the node's weight vector
#
# As an example, to calculate the distance between the vector for the colour red (1, 0, 0) with an arbitrary weight vector (0.1, 0.4, 0.5), do:
#
# $dist = sqrt( (1 - 0.1)^2 + (0 - 0.4)^2+ (0 - 0.5)^2 )$
#
# $dist = sqrt( (0.9)^2 + (-0.4)^2+ (-0.5)^2 )$
#
# $dist = sqrt( 0.81 + 0.16+ 0.25 )$
#
# $dist = sqrt(1.22)$
#
# $dist = 1.106$

# %% [markdown]
# #### Step 4 expanded (BMU's friends):
# * Radius of the neighbourhood shrink over time via:
#
# $\sigma(t) = \sigma_0 exp(-\frac{t}{\lambda}), t=1,2,3...$
#
# Where
# * $t$ is the current time-step (iteration of the loop).
# * $\sigma$ (sigma) denotes the width of the neighborhood, starting at time $t=0$
# * $\lambda$ (lambda) denotes a time constant, such that the neigbborhood shrinks over time.
#
# ![image19.png](./images/image19.png)

# %% [markdown]
# ### Cahoot-11-4
# https://umsystem.instructure.com/courses/97521/quizzes/292481

# %% [markdown]
# #### Step 5 expanded (update weights):
# * Every node within the BMU's neighbourhood (including the BMU) has its weight vector adjusted, according to the following equation:
#
# $W(t + 1) = W(t) + \Theta(t) L(t) (V(t) - W(t))$
#
# Where
# * $t$ represents the time-step
# * $L$ is a small variable called the learning rate, which decreases with time.
#     * With learning rate: $L(t) = L_0 exp(-\frac{t}{\lambda}), t=1,2,3...$
# * Revised $\sigma$, with spatial decay $\Theta$:
#     * $\Theta(t) = exp(-\frac{dist^2}{2\sigma^2(t)}), t=1,2,3...$
#         * Where
#             * t represents the time-step
#             * L is a small variable called the learning rate, which decreases with time. ![image20.png](./images/image20.png)
#
# Basically, what this whole equation is saying, is that the new adjusted weight for the node (W(t+1)) is equal to the old weight (W(t)), plus a fraction of the difference between the old weight (W(t)) and the input vector (V), which decreases in proportion to that distance, and over time.

# %% [markdown]
# #### Cahoot-11-5
# https://umsystem.instructure.com/courses/97521/quizzes/292482

# %% [markdown]
# ### Example dataset: dimensionality reduction on quality of life features
#
# ![image21.png](./images/image21.png)
#
# Notice how countries are not nodes, but match to nodes.
#
# ![image22.png](./images/image22.png)

# %% [markdown]
# ### What kinds of clusters can SOM visualize?
# SOM does better than other clustering algorithms for some problems (and even better than PCA, below)
#
# ![image23.png](./images/image23.png)

# %% [markdown]
# ### Review
# 1. pseudo-code here:
# https://en.wikipedia.org/wiki/Self-organizing_map
#
# 2. Code for color som here:
# [./SOM_color_demo0/som_color.py](./SOM_color_demo0/som_color.py)

# %% [markdown]
# SOM for iris data set
# ---------------------
# * There are four input dimensions:
#     * petal width
#     * petal length
#     * sepal width
#     * sepal length
# * We could use a SOM to group similar flowers near each other in a 2D lattice.
# * To see how good the SOM was at separating the irises into their distinct categories, we apply the iris data to a SOM, and then retrospectively colouring each point with their true class, we get something like this:
#
# ![image24.png](./images/image24.png)
#
# * The unified distance matrix (U-matrix) is a representation of a self-organizing map (SOM), where the Euclidean distance between the codebook vectors of neighboring neurons is depicted in a grayscale image.
# * This image is used to visualize the data in a high-dimensional space using a 2D image.
# * Self organizing map (SOM) of Fisher's Iris Flower Data Set with U-Matrix:
#
# ![image25.png](./images/image25.png)
#
# * Top left: a color image formed by the first three dimensions of the four-dimensional SOM weight vectors.
#     * Would be better as a monochrome of the mean euclidean distances... (as in the cancer paper below).
# * Top Right: a pseudo-color image of the magnitude of the SOM weight vectors.
# * Bottom Left: a U-Matrix (Euclidean distance between weight vectors of neighboring cells) of the SOM.
# * Bottom Right: An overlay of data points on the U-Matrix based on the minimum Euclidean distance between data vectors and SOM weight vectors:
#     * red: I.  setosa,
#     * green: I. versicolor and
#     * blue: I. virginica)
#
# See the code: [./SOM_iris_demo1/SOM_iris_demo1.py](./SOM_iris_demo1/SOM_iris_demo1.py)

# %% [markdown]
# ## Another Iris example
#     Red  = Iris-Setosa
#     Green = Iris-Virginica
#     Blue = Iris-Versicolor
#
# SOM tutorial: http://www.ai-junkie.com/ann/som/som1.html
#
# Dataset source: https://archive.ics.uci.edu/ml/datasets/iris
#
# ![png](./SOM-iris_demo2/output.png)
#
# * Each pixel can be said to represent a node of the SOM.
# * Each pixel is colored based on it's majority winner class.
#     * If a pixel is brightly colored it means a high frequency of the data points that activated that pixel were of that class.
#         * i.e that pixel is the best matching unit for many patterns of the same class.
#     * The contrary is true too.
#         * That is, darker colored pixels are the best matching units for only a few patterns of the class represnted by the color.
# * If a color is a mixture of two (like 'blueish-green'), then the pixel represents a 'gray-area'
#     * i.e that pixel is best matching unit for patterns of different classes.
# * Black pixels are not the best matching pixels for any of the input atterns.
#
# See the code: [./SOM_iris_demo2/SOM_iris_demo2.py](./SOM_iris_demo2/SOM_iris_demo2.py)

# %% [markdown]
# Example: cancer clustering
# ==========================
#
# * [./SOM_cancer_paper.pdf](./SOM_cancer_paper.pdf)
# * http://mct.aacrjournals.org/content/2/3/317
# * http://mct.aacrjournals.org/content/molcanther/2/3/317.full.pdf
#
# ![image26.png](./images/image26.png)
#
# **A**, SOM of filtered dataset.
# * Map dimensions are 38 rows by 23 columns.
# * Map colors indicate Euclidian distance between reference vectors at each map node:
#     * black = close
#     * white = far
#
# **B**, SOM annotated according to the projected node locations for normal tissue gene expressions.
# * Fourteen different tissue types were analyzed across 90 samples: BR, breast; PR, prostate; LG, lung; COR, colorectal; GC, germinal center; BL, bladder; UT, uterine; PL, peripheral lymphocytes; MN, normal monocytes; REN, renal; PAN, pancreas; OV, ovarian; BRA, brain; and CER, cerebellum.
#
# **C**, map projections for the 190 tumor samples.
# * Fourteen different tumor types were analyzed: BR, breast adenocarcinoma; PR, prostate adenocarcinoma; LG, lung adenocarcinoma; CO, colorectal adenocarcinoma; LMA, lymphoma; MEL, melanoma; BL, bladder cell transitional carcinoma; UT, uterine adenocarcinoma; LEU, leukemia; REN, renal cell adenocarcinoma; PAN, pancreatic adenocarcinoma; OV, ovarian adenocarcinoma; MES, pleural mesothelioma; and CNS, CNS.
# * The thick white line represents a boundary between the normal and tumor tissue types.  The location of this boundary is not precise, and its appearance is intended as a reference landmark across all SOMs.
#
# ![image27.png](./images/image27.png)
#
# SOM annotated according to tissue prediction probabilities.
#
# **A** displays the prediction probabilities at each map node, PRi,j, colored spectrally from red (highest probabilities) to blue (lowest probabilities).
# * Map boundaries, shown as a solid white line, indicate separation between normal and tumor samples.
#
# **B and C** identify class predictions of normal and tumor tissues for map nodes.
# * Map nodes are colored according to tissue type and indicated along the figure edge.
#
# ![image28.png](./images/image28.png)
#
# * Class predictions as described in Fig. above (B and C) but annotated according to the 60 clades determined by hierarchical clustering of the SOM reference vectors.
# * An average of 14 ± 9 reference vectors appear in each clade, and the average within clade correlation coefficient between data vectors is 0.86 ± 0.09.
