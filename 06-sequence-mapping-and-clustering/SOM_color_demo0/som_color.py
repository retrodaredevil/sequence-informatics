#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
SOM for random color data
Note, this will take a while to run, and 
will drop tons of images in whatever directory you run it in.
Scroll through those images to see it's performance, 
starting with init.png
"""
import numpy as np
import matplotlib.pyplot as plt  # type: ignore

map_size = 20
# n_iterations = 10000
n_iterations = 1000
np.random.seed(100)


def main() -> None:
    # node array. each node has 3-dim weight vector
    nodes = np.random.rand(map_size, map_size, 3)
    plt.imshow(nodes, interpolation="none")
    plt.savefig("init.png")

    iterations = np.random.rand(n_iterations, 3)
    for i in range(n_iterations):
        train(nodes, iterations, i)
        # out put for i<100 or each 1000 iteration
        if i % 1000 == 0 or i < 100:
            plt.imshow(nodes, interpolation="none")
            plt.savefig(str(i) + ".png")

    plt.imshow(nodes, interpolation="none")
    plt.savefig("final.png")


def train(nodes: np.ndarray, iterations: np.ndarray, i: int) -> None:
    bmu = best_matching_unit(nodes, iterations[i])
    # print bmu
    for x in range(map_size):
        for y in range(map_size):
            # coordinate of unit
            c = np.array([x, y])
            d = np.linalg.norm(c - bmu)
            L = learning_ratio(i)
            S = learning_radius(i, d)
            # TODO clear up using numpy function
            for z in range(3):
                nodes[x, y, z] += L * S * (iterations[i, z] - nodes[x, y, z])


def best_matching_unit(
    nodes: np.ndarray, iterations: np.ndarray
) -> tuple[np.int64, np.int64]:
    # compute all norms (square)
    # TODO simplify using numpy function
    norms = np.zeros((map_size, map_size))
    for i in range(map_size):
        for j in range(map_size):
            for k in range(3):
                norms[i, j] += (nodes[i, j, k] - iterations[k]) ** 2
    # then, choose the minimum one
    # argment with minimum element
    bmu = np.argmin(norms)
    # argmin returns just flatten, serial index,
    # so convert it using unravel_index
    return np.unravel_index(bmu, (map_size, map_size))


def neighbourhood(t: int) -> np.float64:
    # neighbourhood radius
    # for testing
    halflife = float(n_iterations / 4)
    initial = float(map_size / 2)
    return initial * np.exp(-t / halflife)


def learning_ratio(t: int) -> np.float64:
    # for testing
    halflife = float(n_iterations / 4)
    initial = 0.1
    return initial * np.exp(-t / halflife)


def learning_radius(t: int, d: np.float64) -> np.float64:
    # d is distance from BMU
    s = neighbourhood(t)
    return np.exp(-(d**2) / (2 * s**2))


if __name__ == "__main__":
    main()
